package cz.janousek.publictransport.dszo.model;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Martin Janousek on 8.1.18.
 * - email: mjanousek92@gmail.com
 */
public class ActualInfoTest {

    @Test
    public void testEquals() throws Exception {
    }

    @Test
    public void testHashCode() throws Exception {
        ActualInfo ai1 = createActulaInfo1();
        ActualInfo ai2 = createActulaInfo2();
        ActualInfo ai3 = createActulaInfo1();

        assertEquals("Objects are equal.", ai1, ai1);
        assertEquals("Objects are equal.", ai1, ai3);
        assertNotEquals("Objects are not equal.", ai1, ai2);
    }

    @Test
    public void testHashCodeList() throws Exception {
        ActualInfo ai1 = createActulaInfo1();
        ActualInfo ai2 = createActulaInfo2();
        ActualInfo ai3 = createActulaInfo1();

        List<ActualInfo> l1 = new LinkedList();
        List<ActualInfo> l2 = new LinkedList();

        l1.add(ai1);
        l2.add(ai1);
        assertEquals("Lists are equal.", l1, l2);

        l1.add(ai2);
        assertNotEquals("Lists are not equal.", l1, l2);

        l2.add(ai2);
        assertEquals("Lists are equal.", l1, l2);

        l1.add(ai2);
        l2.add(ai1);
        assertNotEquals("Lists are not equal.", l1, l2);
    }

    private ActualInfo createActulaInfo1() {
        return new ActualInfo(
                "001", "12", 7, 5, 8, 1, "426",
                "45", "456", "-49.456123",
                "17.123123", "16:45:12", 123
        );
    }

    private ActualInfo createActulaInfo2() {
        return new ActualInfo(
                "051", "12", 7, 5, 8, 1, "486",
                "40", "456", "-49.000123",
                "17.120023", "16:21:12", 123
        );
    }

}