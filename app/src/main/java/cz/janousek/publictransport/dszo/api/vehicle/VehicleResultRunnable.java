package cz.janousek.publictransport.dszo.api.vehicle;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.api.ResultRunnable;
import cz.janousek.publictransport.dszo.model.Vehicle;

/**
 * Created by martin on 25.10.17.
 */

public class VehicleResultRunnable extends ResultRunnable<Vehicle> {

    public VehicleResultRunnable(DSZOApplication dszoApplication) {
        super(dszoApplication);
        this.dao = dszoApplication.getDatabaseHelper().getVehicleDao();
        lastSyncString = dszoApplication.getString(R.string.LAST_SYNC_VEHICLE);
    }
}
