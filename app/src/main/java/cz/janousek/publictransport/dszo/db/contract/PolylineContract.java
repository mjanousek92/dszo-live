package cz.janousek.publictransport.dszo.db.contract;

import android.provider.BaseColumns;

/**
 * Created by martin on 24.10.17.
 */

public final class PolylineContract {

    private PolylineContract() {}

    public static class Sql implements BaseColumns {
        public static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                        Entry.COLUMN_NAME_ID + " TEXT, " +
                        Entry.COLUMN_NAME_POLYLINE + " TEXT, " +
                        Entry.COLUMN_NAME_LEVELS + " TEXT, " +
                        "PRIMARY KEY (" + Entry.COLUMN_NAME_ID + ")" +
                    ")";
    }

    public static class Entry implements BaseColumns {
        public static final String TABLE_NAME = "PolyLine";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_POLYLINE = "polyline";
        public static final String COLUMN_NAME_LEVELS = "levels";
    }
}
