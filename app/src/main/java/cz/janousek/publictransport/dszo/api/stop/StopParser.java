package cz.janousek.publictransport.dszo.api.stop;

import java.util.LinkedList;
import java.util.List;

import cz.janousek.publictransport.dszo.api.ArrayParser;
import cz.janousek.publictransport.dszo.model.Stop;

/**
 * Created by martin on 23.10.17.
 */
public class StopParser implements ArrayParser<Stop> {

    @Override
    public List<Stop> parse(List<String> data) {
        List result = new LinkedList();
        for (String line : data) {
            String[] a = line.split(";");
            Stop stop = new Stop(
                a[FieldOrder.PASSPORT],
                a[FieldOrder.COLUMN],
                a[FieldOrder.NAME],
                a[FieldOrder.LAT],
                a[FieldOrder.LNG],
                TRUE.equals(a[FieldOrder.ON_REQUEST]),
                TRUE.equals(a[FieldOrder.ON_REQUEST_IN_SPECIFICTIME]),
                TRUE.equals(a[FieldOrder.FRONT_DOOR_ONLY]),
                TRUE.equals(a[FieldOrder.TICKET_MACHINE]),
                a[FieldOrder.ZONE],
                a[FieldOrder.STATE_STOP_NUMBER]
            );
            result.add(stop);
        }
        return result;
    }

    private interface FieldOrder{
        int PASSPORT = 0;
        int COLUMN = 1;
        int NAME = 2;
        int LAT = 3;
        int LNG = 4;
        int ON_REQUEST = 5;
        int ON_REQUEST_IN_SPECIFICTIME = 6;
        int FRONT_DOOR_ONLY = 7;
        int TICKET_MACHINE = 8;
        int ZONE = 9;
        int STATE_STOP_NUMBER = 10;
    }
}
