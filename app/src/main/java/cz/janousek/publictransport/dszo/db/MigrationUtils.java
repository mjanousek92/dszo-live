package cz.janousek.publictransport.dszo.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import cz.janousek.publictransport.dszo.db.contract.ActualInfoContract;
import cz.janousek.publictransport.dszo.db.contract.PolylineContract;
import cz.janousek.publictransport.dszo.db.contract.VehicleContract;

/**
 * Created by martin on 27.11.17.
 */

class MigrationUtils {

    public static void migrate(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (int i = oldVersion + 1; i <= newVersion; i++) {
            Log.d("MigrateUtils:migrate", "Iteration: " + i);
            migrateTo(db, i);
        }
    }

    private static void migrateTo(SQLiteDatabase db, int newVersion) {
        switch (newVersion) {
            case 4:
                db.execSQL(PolylineContract.Sql.SQL_CREATE_TABLE);
                break;
            case 3:
                db.execSQL(ActualInfoContract.Sql.SQL_CREATE_TABLE);
                break;
            case 2:
                db.execSQL(VehicleContract.Sql.SQL_CREATE_TABLE);
                break;
            default:
                break;
        }
    }
}
