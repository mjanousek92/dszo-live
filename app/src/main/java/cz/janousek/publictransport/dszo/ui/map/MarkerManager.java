package cz.janousek.publictransport.dszo.ui.map;

import android.app.Activity;
import android.content.Context;

import com.annimon.stream.Stream;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import cz.janousek.publictransport.dszo.MyDrawableUtils;
import cz.janousek.publictransport.dszo.model.CompoundActualInfo;
import cz.janousek.publictransport.dszo.model.MarkerTag;
import cz.janousek.publictransport.dszo.model.resourceSet.ResourceSet;
import cz.janousek.publictransport.dszo.model.resourceSet.ResourceSetFactory;

/**
 * Created by martin on 5.11.17.
 */

public class MarkerManager {
    private Map<String, Marker> markers;
    private Set<String> existingMarkersIds;
    private MyDrawableUtils myDrawableUtils;

    public MarkerManager(Context context) {
        init(context);
    }

    public void addMarkers(GoogleMap map, List<CompoundActualInfo> data, Activity activity){
        Set<String> newMarkersIds = new TreeSet<>();
        for (CompoundActualInfo compoundActualInfo : data) {
            MarkerTag markertag = createMarkerTag(compoundActualInfo);
            addMarker(map, markertag, activity);
            newMarkersIds.add(compoundActualInfo.getVehicleNumber());
        }
        removeNonExistingMarkers(newMarkersIds);
    }

    private MarkerTag createMarkerTag(CompoundActualInfo compoundActualInfo) {
        MarkerTag markerTag = new MarkerTag(compoundActualInfo);
        return markerTag;
    }

    private void removeNonExistingMarkers(Set<String> newMarkersIds) {
        existingMarkersIds.removeAll(newMarkersIds);
        removeMarkers(existingMarkersIds);
        existingMarkersIds = newMarkersIds;
    }


    private void removeMarkers(Set<String> existingMarkersIds) {
        Stream.of(existingMarkersIds).forEach(this::removeMarker);
    }

    private void removeMarker(String id) {
        Marker marker = markers.get(id);
        marker.remove(); //remove from map
        markers.remove(id);
    }

    // TODO refactor addMarker and updateMarker
    public void addMarker(GoogleMap map, MarkerTag markerTag, Activity activity) {
        ResourceSet resourceSet = ResourceSetFactory.createResourceSet(markerTag.getCompoundActualInfo().getVehicleTraction());
        if (!existingMarkersIds.contains(markerTag.getCompoundActualInfo().getVehicleNumber())) {
            createMarker(map, markerTag, activity, resourceSet);
        } else {
            updateMarkerInMap(map, markerTag, activity, resourceSet);
        }
    }

    private void updateMarkerInMap(GoogleMap map, MarkerTag markerTag, Activity activity, ResourceSet resourceSet) {
        Marker marker = markers.get(markerTag.getCompoundActualInfo().getVehicleNumber());
        if (marker != null) {
            updateMarker(markerTag, activity, resourceSet, marker);
        } else {
            createMarker(map, markerTag, activity, resourceSet);
        }
    }

    private void createMarker(GoogleMap map, MarkerTag markerTag, Activity activity, ResourceSet resourceSet) {
        Marker marker = addMarkerToMap(map, markerTag, activity, resourceSet);
        markers.put(markerTag.getCompoundActualInfo().getVehicleNumber(), marker);
    }

    private Marker addMarkerToMap(GoogleMap map, MarkerTag markerTag, Activity activity, ResourceSet resourceSet) {
        if(map == null) {
            return null;
        }

        Marker marker = map.addMarker(new MarkerOptions()
            .position(markerTag.getCompoundActualInfo().getLatLng())
            .rotation(markerTag.getCompoundActualInfo().getAzimuth())
            .title("Linka "+ markerTag.getCompoundActualInfo().getBusLineNumber())
            .icon(myDrawableUtils.writeOnDrawable(
                    activity, resourceSet, String.valueOf(markerTag.getCompoundActualInfo().getBusLineNumber()), markerTag.getCompoundActualInfo().getAzimuth())
            )
            .anchor(0.5f, 0.5f)
            .infoWindowAnchor(0.5f, 0)
        );
        marker.setTag(markerTag);
        return marker;
    }

    private void updateMarker(MarkerTag markerTag, Activity activity, ResourceSet resourceSet, Marker marker) {
        marker.setPosition(markerTag.getCompoundActualInfo().getLatLng());
        marker.setRotation(markerTag.getCompoundActualInfo().getAzimuth());
        marker.setIcon(myDrawableUtils.writeOnDrawable(
                activity, resourceSet, String.valueOf(markerTag.getCompoundActualInfo().getBusLineNumber()), markerTag.getCompoundActualInfo().getAzimuth())
        );
        marker.setAnchor(0.5f, 0.5f);
        marker.setInfoWindowAnchor(0.5f, 0);
    }

    private void init(Context context) {
        markers = new HashMap<>();
        existingMarkersIds = new TreeSet<>();
        myDrawableUtils = new MyDrawableUtils(context);
    }

    public Marker get(String vehicleId) {
        if (markers != null) {
            return markers.get(vehicleId);
        }
        return null;
    }
}
