package cz.janousek.publictransport.dszo.model;

/**
 * Created by Martin Janousek on 8.1.18.
 * - email: mjanousek92@gmail.com
 */

public class Polyline {
    private String id;
    private String polyline;
    private String levels;

    public Polyline(String id, String polyline, String levels) {
        this.id = id;
        this.polyline = polyline;
        this.levels = levels;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPolyline() {
        return polyline;
    }

    public void setPolyline(String polyline) {
        this.polyline = polyline;
    }

    public String getLevels() {
        return levels;
    }

    public void setLevels(String levels) {
        this.levels = levels;
    }
}
