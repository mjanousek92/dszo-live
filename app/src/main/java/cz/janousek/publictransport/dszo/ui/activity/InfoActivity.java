package cz.janousek.publictransport.dszo.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.ui.factories.info.Factories;
import cz.janousek.publictransport.dszo.ui.renderer.info.Renderer;

public class InfoActivity extends AppCompatActivity {

    private Factories factories;
    private Renderer renderer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        factories = new Factories(this);
        renderer = factories.getRenderer();
        renderer.render();
    }

    @Override
    protected void onResume() {
        super.onResume();

        renderer.setActiveMenuItem();
    }
}
