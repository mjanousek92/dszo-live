package cz.janousek.publictransport.dszo.api.synchronizer;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.api.ActualInfo.ActualInfoParser;
import cz.janousek.publictransport.dszo.model.ActualInfo;

import static cz.janousek.publictransport.dszo.utils.MilisecondIntervals.SECOND;

/**
 * Created by martin on 30.12.17.
 */

public class ActualInfoSynchronizer extends Synchronizer<ActualInfo>{
    private static final long SYNCHRONIZATION_INTERVAL = 50 * SECOND;

    public ActualInfoSynchronizer(DSZOApplication dszoApplication) {
        super(dszoApplication);
        resultRunnable = dszoApplication.getActualInfoResultRunnable();
        parser = new ActualInfoParser();
        url = "https://www.dszo.cz/online/odchylky.txt";
        lastSyncString = dszoApplication.getString(R.string.LAST_SYNC_ACTUAL_INFO);
        synchoronizationInterval = SYNCHRONIZATION_INTERVAL;
    }
}
