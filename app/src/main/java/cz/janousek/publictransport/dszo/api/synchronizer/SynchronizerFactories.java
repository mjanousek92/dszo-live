package cz.janousek.publictransport.dszo.api.synchronizer;

import cz.janousek.publictransport.dszo.DSZOApplication;

/**
 * Created by martin on 30.12.17.
 */

public class SynchronizerFactories {

    private StopSynchronizer stopSynchronizer;
    private VehicleSynchronizer vehicleSynchronizer;
    private ActualInfoSynchronizer actualInfoSynchronizer;

    public StopSynchronizer getStopSynchronizer(DSZOApplication dszoApplication) {
        if(stopSynchronizer == null) {
            stopSynchronizer = new StopSynchronizer(dszoApplication);
        }
        return stopSynchronizer;
    }

    public VehicleSynchronizer getVehicleSynchronizer(DSZOApplication dszoApplication) {
        if(vehicleSynchronizer == null) {
            vehicleSynchronizer = new VehicleSynchronizer(dszoApplication);
        }
        return vehicleSynchronizer;
    }

    public ActualInfoSynchronizer getActualDataSynchronizer(DSZOApplication dszoApplication) {
        if(actualInfoSynchronizer == null) {
            actualInfoSynchronizer = new ActualInfoSynchronizer(dszoApplication);
        }
        return actualInfoSynchronizer;
    }
}
