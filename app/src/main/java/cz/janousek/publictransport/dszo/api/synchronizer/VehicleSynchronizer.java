package cz.janousek.publictransport.dszo.api.synchronizer;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.api.vehicle.VehicleParser;
import cz.janousek.publictransport.dszo.model.Vehicle;

import static cz.janousek.publictransport.dszo.utils.MilisecondIntervals.MINUTE;

/**
 * Created by martin on 25.10.17.
 */

public class VehicleSynchronizer extends Synchronizer<Vehicle> {
    private static final long SYNCHRONIZATION_INTERVAL = MINUTE;

    public VehicleSynchronizer(DSZOApplication dszoApplication) {
        super(dszoApplication);
        resultRunnable = dszoApplication.getVehicleResultRunnable();
        parser = new VehicleParser();
        url = "https://www.dszo.cz/online/vozy.php";
        lastSyncString = dszoApplication.getString(R.string.LAST_SYNC_VEHICLE);
        synchoronizationInterval = SYNCHRONIZATION_INTERVAL;
    }
}
