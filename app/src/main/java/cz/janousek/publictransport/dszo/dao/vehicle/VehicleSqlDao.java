package cz.janousek.publictransport.dszo.dao.vehicle;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.List;

import cz.janousek.publictransport.dszo.dao.BaseDao;
import cz.janousek.publictransport.dszo.db.DatabaseHelper;
import cz.janousek.publictransport.dszo.db.contract.VehicleContract;
import cz.janousek.publictransport.dszo.model.Vehicle;

import static cz.janousek.publictransport.dszo.api.ArrayParser.TRUE;

/**
 * Created by martin on 23.10.17.
 */

public class VehicleSqlDao extends BaseDao implements VehicleDao {
    public VehicleSqlDao(DatabaseHelper dh) {
        super(dh);
    }

    @Override
    public List<Vehicle> getAllVehicles() {
        return null;
    }

    @Override
    public boolean insertOrUpdateAll(List<Vehicle> vehicles) {
        SQLiteDatabase db = dh.getWritableDatabase();

        for (Vehicle vehicle : vehicles) {
            ContentValues values = createContentValues(vehicle);

            // update
            long id = db.update(
                    VehicleContract.Entry.TABLE_NAME,
                    values,
                    VehicleContract.Entry.COLUMN_NAME_NUMBER + "=?",
                    new String[]{vehicle.getNumber()}
            );

            //insert row unless row was updated
            if (id == 0) {
                // TODO tady teoreticky neni potreba update.. Melo by to umet nahradit, pokud se to spravne nastavi. To same i ve Vehicles.
                id = db.insertWithOnConflict(VehicleContract.Entry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
//                Log.d("DB", "Inserted id: " + id);
            } else {
//                Log.d("DB", "Updated id: " + id);
            }
        }
        return true;
    }

    @Override
    public Vehicle get(String number) {
        SQLiteDatabase db = dh.getReadableDatabase();

        String[] projection = {
                VehicleContract.Entry.COLUMN_NAME_NUMBER,
                VehicleContract.Entry.COLUMN_NAME_TRACTION,
                VehicleContract.Entry.COLUMN_NAME_TYPE,
                VehicleContract.Entry.COLUMN_NAME_LOW_FLOOR,
                VehicleContract.Entry.COLUMN_NAME_AIR_CONDITION,
                VehicleContract.Entry.COLUMN_NAME_WIFI,
                VehicleContract.Entry.COLUMN_NAME_LCD,
                VehicleContract.Entry.COLUMN_NAME_BLIND_SUPPORT
        };

        String selection = VehicleContract.Entry.COLUMN_NAME_NUMBER + " = ?";
        String[] selectionArgs = {number};

        Cursor cursor = db.query(
                VehicleContract.Entry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        Vehicle stop = null;

        if (cursor.moveToFirst()) {
            stop = new Vehicle(
                cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_NUMBER)),
                cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_TRACTION)),
                cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_TYPE)),
                TRUE.equals(cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_LOW_FLOOR))),
                TRUE.equals(cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_AIR_CONDITION))),
                TRUE.equals(cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_WIFI))),
                TRUE.equals(cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_LCD))),
                TRUE.equals(cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_BLIND_SUPPORT)))
            );
        }

        return stop;
    }

    @NonNull
    private ContentValues createContentValues(Vehicle vehicle) {
        ContentValues values = new ContentValues();
        values.put(VehicleContract.Entry.COLUMN_NAME_NUMBER, vehicle.getNumber());
        values.put(VehicleContract.Entry.COLUMN_NAME_TRACTION, vehicle.getTraction());
        values.put(VehicleContract.Entry.COLUMN_NAME_TYPE, vehicle.getType());
        values.put(VehicleContract.Entry.COLUMN_NAME_LOW_FLOOR, vehicle.isLowFloor());
        values.put(VehicleContract.Entry.COLUMN_NAME_AIR_CONDITION, vehicle.isAirCondition());
        values.put(VehicleContract.Entry.COLUMN_NAME_WIFI, vehicle.isWifi());
        values.put(VehicleContract.Entry.COLUMN_NAME_LCD, vehicle.isLcd());
        values.put(VehicleContract.Entry.COLUMN_NAME_BLIND_SUPPORT, vehicle.isBlindSupport());
        return values;
    }
}
