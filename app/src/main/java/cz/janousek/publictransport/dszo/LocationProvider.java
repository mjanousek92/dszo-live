package cz.janousek.publictransport.dszo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;

import cz.janousek.publictransport.dszo.ui.listener.MapListener;
import cz.janousek.publictransport.dszo.utils.ErrorLogger;

/**
 * Created by martin on 29.10.17.
 */

public class LocationProvider {

    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private FusedLocationProviderClient mFusedLocationProviderClient;
    private GoogleApiClient mGoogleApiClient;
    private LocationCallback locationCallback;

    private LocationRequest mLocationRequest;
    private Location mLastKnownLocation;

    private Location mCameraPosition;
    private MapListener mapListener;
    private final Activity activity;

    private boolean locationPermissionGranted;
    private Location location;
    private boolean centerAfterResponse;

    public LocationProvider(Activity activity) {
        this.activity = activity;
    }

    public void setDependencies(MapListener mapListener, FusedLocationProviderClient fusedLocationProviderClient, LocationCallback locationCallback){
        this.mapListener = mapListener;
        this.mFusedLocationProviderClient = fusedLocationProviderClient;
        this.locationCallback = locationCallback;
    }

    public void onCreate(Bundle savedInstanceState){
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }
    }

    public void onResume(){
        if (mGoogleApiClient != null && mFusedLocationProviderClient != null) {
            requestLocationUpdates();
        } else {
            ErrorLogger.log(ErrorLogger.HIGH, this.getClass().getSimpleName(), "GoogleApiClient or LocationProviderClient is null");
        }
    }

    public void onPause() {
        if (mFusedLocationProviderClient != null) {
            mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
            mapListener.enableMyLocation(false);
        }
    }

    public void requestLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, locationCallback, Looper.myLooper());
        } else {
            ActivityCompat.requestPermissions(
                activity,
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            );
        }
    }

    public void setLocation(Location location) {
        if(location != null) {
            if (centerAfterResponse) {
                centerAfterResponse = false;
                mapListener.moveMap(location);
            }
            mapListener.enableMyLocation(true);
        }
    }

    public void isCenterAfterResponse(boolean enable) {
        this.centerAfterResponse = enable;
    }
}
