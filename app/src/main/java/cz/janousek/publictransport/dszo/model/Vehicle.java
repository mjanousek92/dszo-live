package cz.janousek.publictransport.dszo.model;

/**
 * Created by martin on 23.10.17.
 */

public class Vehicle {

    public static final String BUS = "aut";
    public static final String TROLLEY = "tr";
    public static final String VEHICLE_ID = "vehicle_id";

    private String number;
    private String traction;
    private String type;
    private boolean lowFloor;
    private boolean airCondition;
    private boolean wifi;
    private boolean lcd;
    private boolean blindSupport;

    public Vehicle(
        String number,
        String traction,
        String type,
        boolean lowFloor,
        boolean airCondition,
        boolean wifi,
        boolean lcd,
        boolean blindSupport
    ) {
        this.number = number;
        this.traction = traction;
        this.type = type;
        this.lowFloor = lowFloor;
        this.airCondition = airCondition;
        this.wifi = wifi;
        this.lcd = lcd;
        this.blindSupport = blindSupport;
    }

    public String getNumber() {
        return number;
    }

    public String getTraction() {
        return traction;
    }

    public String getType() {
        return type;
    }

    public boolean isLowFloor() {
        return lowFloor;
    }

    public boolean isAirCondition() {
        return airCondition;
    }

    public boolean isWifi() {
        return wifi;
    }

    public boolean isLcd() {
        return lcd;
    }

    public boolean isBlindSupport() {
        return blindSupport;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setTraction(String traction) {
        this.traction = traction;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setLowFloor(boolean lowFloor) {
        this.lowFloor = lowFloor;
    }

    public void setAirCondition(boolean airCondition) {
        this.airCondition = airCondition;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public void setLcd(boolean lcd) {
        this.lcd = lcd;
    }

    public void setBlindSupport(boolean blindSupport) {
        this.blindSupport = blindSupport;
    }
}
