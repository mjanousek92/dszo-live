package cz.janousek.publictransport.dszo.ui.dialog.sort;

import android.content.Context;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.db.contract.ActualInfoContract;
import cz.janousek.publictransport.dszo.ui.dialog.Option;
import cz.janousek.publictransport.dszo.ui.dialog.Options;

/**
 * Created by Martin Janousek on 6.1.18.
 * - email: mjanousek92@gmail.com
 */

public class SortOprions extends Options {

    public SortOprions(Context context) {
        super(context);
        init();
    }

    private void init() {
        options.add(new Option(context.getString(R.string.bus_line), ActualInfoContract.Entry.COLUMN_NAME_LINE));
        options.add(new Option(context.getString(R.string.delay), ActualInfoContract.Entry.COLUMN_NAME_DELAY));
    }
}
