package cz.janousek.publictransport.dszo.dao.polyline;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.List;

import cz.janousek.publictransport.dszo.dao.BaseDao;
import cz.janousek.publictransport.dszo.db.DatabaseHelper;
import cz.janousek.publictransport.dszo.db.contract.PolylineContract;
import cz.janousek.publictransport.dszo.model.Polyline;

/**
 * Created by martin on 23.10.17.
 */

public class PolylineSqlDao extends BaseDao implements PolylineDao {

    public PolylineSqlDao(DatabaseHelper dh) {
        super(dh);
    }

    @Override
    public void insert(Polyline polyline) {
        SQLiteDatabase db = dh.getWritableDatabase();
        ContentValues values = createContentValues(polyline);
        long newRowId = db.insert(PolylineContract.Entry.TABLE_NAME, null, values);
    }

    @Override
    public boolean insertAll(List<Polyline> polylines) {
        SQLiteDatabase db = dh.getWritableDatabase();

        for (Polyline polyline : polylines) {
            ContentValues values = createContentValues(polyline);

            // update
            long id = db.update(
                    PolylineContract.Entry.TABLE_NAME,
                    values,
                    PolylineContract.Entry.COLUMN_NAME_ID + "=?",
                    new String[]{polyline.getId()}
            );

            if (id == 0) {
                id = db.insertWithOnConflict(PolylineContract.Entry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            }
        }
        return true;
    }

    @NonNull
    private ContentValues createContentValues(Polyline polyline) {
        ContentValues values = new ContentValues();
        values.put(PolylineContract.Entry.COLUMN_NAME_ID, polyline.getId());
        values.put(PolylineContract.Entry.COLUMN_NAME_POLYLINE, polyline.getPolyline());
        values.put(PolylineContract.Entry.COLUMN_NAME_LEVELS, polyline.getLevels());
        return values;
    }

    @Override
    public boolean insertOrUpdateAll(List<Polyline> data) {
        return false;
    }
}
