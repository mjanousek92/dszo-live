package cz.janousek.publictransport.dszo.ui.listener;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.core.action.Action;
import cz.janousek.publictransport.dszo.core.action.OpenActivityAction;
import cz.janousek.publictransport.dszo.core.action.RateAction;
import cz.janousek.publictransport.dszo.core.action.RateDialogAction;
import cz.janousek.publictransport.dszo.core.action.ShareAction;
import cz.janousek.publictransport.dszo.ui.activity.InfoActivity;
import cz.janousek.publictransport.dszo.ui.activity.ListActivity;
import cz.janousek.publictransport.dszo.ui.activity.MapActivity;

/**
 * Created by martin on 22.10.17.
 */

public class NavigationItemListener implements NavigationView.OnNavigationItemSelectedListener {

    private Activity activity;
    private DrawerLayout drawer;

    public NavigationItemListener(Activity activity) {
        this.activity = activity;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Action action = null;
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_info:
                action = new OpenActivityAction(activity, InfoActivity.class);
                break;
            case R.id.nav_list:
                action = new OpenActivityAction(activity, ListActivity.class);
                break;
            case R.id.nav_map:
                action = new OpenActivityAction(activity, MapActivity.class);
                break;
            case R.id.nav_share:
                action = new ShareAction(activity);
                break;
            case R.id.nav_rate:
                action = new RateDialogAction(activity, new RateAction(activity));
                break;
            default:
                break;
        }

        if (action != null) {
            action.invoke();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setDrawer(DrawerLayout drawer) {
        this.drawer = drawer;
    }

}
