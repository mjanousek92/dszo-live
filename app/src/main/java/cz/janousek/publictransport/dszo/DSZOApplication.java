package cz.janousek.publictransport.dszo;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;

import cz.janousek.publictransport.dszo.api.ActualInfo.ActualInfoResultRunnable;
import cz.janousek.publictransport.dszo.api.ResultRunnable;
import cz.janousek.publictransport.dszo.api.stop.StopResultRunnable;
import cz.janousek.publictransport.dszo.api.synchronizer.AllDataSynchronizer;
import cz.janousek.publictransport.dszo.api.vehicle.VehicleResultRunnable;
import cz.janousek.publictransport.dszo.db.DatabaseHelper;
import io.fabric.sdk.android.Fabric;

/**
 * Created by martin on 22.10.17.
 */

public class DSZOApplication extends Application{
    public static String APPLICATION_PREFFERENCES = "APPLICATION_PREFFERENCES";

    private DatabaseHelper databaseHelper;
    private AllDataSynchronizer allDataSynchronizer;
    private ResultRunnable actualInfoResultRunnable;
    private VehicleResultRunnable vehicleResultRunnable;
    private StopResultRunnable stopResultRunnable;

    public DatabaseHelper getDatabaseHelper() {
        if(databaseHelper == null) {
            databaseHelper = new DatabaseHelper(this);
        }
        return databaseHelper;
    }

    public AllDataSynchronizer getAllDataSynchronizer() {
        if(allDataSynchronizer == null) {
            allDataSynchronizer = new AllDataSynchronizer(this);
        }
        return allDataSynchronizer;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        Fabric.with(this, new Crashlytics());
    }

    public ResultRunnable getActualInfoResultRunnable() {
        if (actualInfoResultRunnable == null) {
            actualInfoResultRunnable = new ActualInfoResultRunnable(this);
        }
        return actualInfoResultRunnable;
    }

    public ResultRunnable getVehicleResultRunnable() {
        if (vehicleResultRunnable == null) {
            vehicleResultRunnable = new VehicleResultRunnable(this);
        }
        return vehicleResultRunnable;
    }

    public ResultRunnable getStopResultRunnable() {
        if (stopResultRunnable == null) {
            stopResultRunnable = new StopResultRunnable(this);
        }
        return stopResultRunnable;
    }

    //
//    public Gson getGson(){
//        if(gson == null) {
//            gson = (new GsonBuilder())
//                    .create();
//        }
//        return gson;
//    }
//
//    public Parser getParser() {
//        if(parser == null) {
//            parser = new Parser(getGson());
//        }
//        return parser;
//    }
//
//    public Retrofit getRetrofitBuilder(){
//        if(retrofit == null) {
//            retrofit = new Retrofit.Builder()
//                    .baseUrl("http://trickortreat-justincase.rhcloud.com")
//                    .addConverterFactory(buildGsonConverter(getGson()))
//                    .build();
//        }
//        return retrofit;
//    }
//
//    private GsonConverterFactory buildGsonConverter(Gson gson) {
//        return GsonConverterFactory.create(gson);
//    }
//
//    public Api getApiService(){
//        if(apiService == null){
//            apiService = getRetrofitBuilder().create(Api.class);
//        }
//        return apiService;
//    }
}

