package cz.janousek.publictransport.dszo.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import cz.janousek.publictransport.dszo.DSZOApplication;

/**
 * Created by martin on 30.12.17.
 */

public class BaseActivity extends AppCompatActivity {

    DSZOApplication dszo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dszo = (DSZOApplication) getApplication();
    }

    public DSZOApplication getDszo() {
        return dszo;
    }
}
