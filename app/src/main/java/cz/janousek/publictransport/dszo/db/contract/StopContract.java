package cz.janousek.publictransport.dszo.db.contract;

import android.provider.BaseColumns;

/**
 * Created by martin on 24.10.17.
 */

public final class StopContract {

    private StopContract() {}

    // TODO: Change Text type to Integer for particular fields
    public static class StopSql implements BaseColumns {
        public static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + StopEntry.TABLE_NAME + " (" +
                        StopEntry.COLUMN_NAME_PASSPORT + " TEXT, " +
                        StopEntry.COLUMN_NAME_COLUMN + " TEXT, " +
                        StopEntry.COLUMN_NAME_NAME + " TEXT, " +
                        StopEntry.COLUMN_NAME_LAT + " TEXT, " +
                        StopEntry.COLUMN_NAME_LNG + " TEXT, " +
                        StopEntry.COLUMN_NAME_ON_REQUEST + " BOOLEAN, " +
                        StopEntry.COLUMN_NAME_ON_REQUEST_IN_SPECIFICTIME + " BOOLEAN, " +
                        StopEntry.COLUMN_NAME_FRONT_DOOR_ONLY + " BOOLEAN, " +
                        StopEntry.COLUMN_NAME_TICKET_MACHINE + " BOOLEAN, " +
                        StopEntry.COLUMN_NAME_ZONE + " TEXT, " +
                        StopEntry.COLUMN_NAME_STATE_STOP_NUMBER + " TEXT, " +
                        "PRIMARY KEY (" + StopEntry.COLUMN_NAME_PASSPORT + ", " + StopEntry.COLUMN_NAME_COLUMN + ")" +
                    ")";
    }

    public static class StopEntry implements BaseColumns {
        public static final String TABLE_NAME = "Stop";
        public static final String COLUMN_NAME_PASSPORT = "passport";
        public static final String COLUMN_NAME_COLUMN = "column";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_LAT = "lat";
        public static final String COLUMN_NAME_LNG = "lng";
        public static final String COLUMN_NAME_ON_REQUEST = "onRequest";
        public static final String COLUMN_NAME_ON_REQUEST_IN_SPECIFICTIME = "onRequestInSpecifictime";
        public static final String COLUMN_NAME_FRONT_DOOR_ONLY = "frontDoorOnly";
        public static final String COLUMN_NAME_TICKET_MACHINE = "ticketMachine";
        public static final String COLUMN_NAME_ZONE = "zone";
        public static final String COLUMN_NAME_STATE_STOP_NUMBER = "stateStopNumber";
    }
}
