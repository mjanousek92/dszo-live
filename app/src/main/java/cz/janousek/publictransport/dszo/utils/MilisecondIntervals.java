package cz.janousek.publictransport.dszo.utils;

/**
 * Created by Martin Janousek on 7.1.18.
 * - email: mjanousek92@gmail.com
 */

public class MilisecondIntervals {
    public static final long SECOND = 1000;
    public static final long MINUTE = 60 * SECOND;
    public static final long HOUR = 60 * MINUTE;
    public static final long DAY = 24 * HOUR;
    public static final long WEEK = 7 * DAY;
}
