package cz.janousek.publictransport.dszo.ui.factories.map;

import android.app.Activity;
import android.app.FragmentManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.opencsv.CSVParser;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.LocationProvider;
import cz.janousek.publictransport.dszo.ui.activity.BaseActivity;
import cz.janousek.publictransport.dszo.ui.dialog.OnOptionClickListener;
import cz.janousek.publictransport.dszo.ui.dialog.filter.OnFilterOptionClickListener;
import cz.janousek.publictransport.dszo.ui.fragment.MapFragment;
import cz.janousek.publictransport.dszo.ui.listener.MapListener;
import cz.janousek.publictransport.dszo.ui.listener.NavigationItemListener;
import cz.janousek.publictransport.dszo.ui.listener.OnMyLocationClickListener;
import cz.janousek.publictransport.dszo.ui.map.InfoWindowAdapter;
import cz.janousek.publictransport.dszo.ui.map.InfoWindowRenderer;
import cz.janousek.publictransport.dszo.ui.renderer.map.Renderer;
import cz.janousek.publictransport.dszo.utils.ErrorLogger;

/**
 * Created by martin on 22.10.17.
 */

public class Factories {
    private BaseActivity activity;
    private final DSZOApplication dszo;
    private Renderer renderer;
    private NavigationItemListener navigationItemListener;
    private CSVParser csvParser;
    private FragmentManager fragmentManager;
    private MapListener mapListener;
    private MapFragment mapFragment;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationProvider locationProvider;
    private View.OnClickListener onMyLocationClockListener;
    private InfoWindowAdapter infoWindowAdapter;
    private LocationCallback locationCallback;
    private GoogleApiClient googleApiClient;
    private GoogleApiClient.ConnectionCallbacks connectionCallbacks;
    private GoogleApiClient.OnConnectionFailedListener connectionFailedListener;
    private InfoWindowRenderer infoWindowRenderer;
    private OnFilterOptionClickListener onFilterOptionClickListener;

    public Factories(BaseActivity activity, DSZOApplication dszo) {
        this.activity = activity;
        this.dszo = dszo;
    }

    public Renderer getRenderer() {
        if (renderer == null) {
            renderer = new Renderer(
                    activity,
                    getNavigationItemListener(),
                    getMapFragment(),
                    getFragmentManager(),
                    getOnMyLocationClockListener()
            );
        }
        return renderer;
    }

    public NavigationItemListener getNavigationItemListener() {
        if(navigationItemListener == null) {
            navigationItemListener = new NavigationItemListener(activity);
        }
        return navigationItemListener;
    }

    public FragmentManager getFragmentManager() {
        if(fragmentManager == null) {
            fragmentManager = activity.getFragmentManager();
        }
        return fragmentManager;
    }

    public MapListener getMapListener() {
        if (mapListener == null) {
            mapListener = new MapListener(activity);
            mapListener.setDependencies(
                    getLocationProvider(),
                    getRenderer(),
                    getInfoWindowAdapter()
            );
        }
        return mapListener;
    }

    public MapFragment getMapFragment() {
        if(mapFragment == null) {
            mapFragment = new MapFragment();
            mapFragment.setDependencies(getMapListener());
        }
        return mapFragment;
    }

    public FusedLocationProviderClient getFusedLocationProviderClient() {
        if (fusedLocationProviderClient == null) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
        }
        return fusedLocationProviderClient;
    }

    public LocationProvider getLocationProvider() {
        if (locationProvider == null) {
            locationProvider = new LocationProvider(
                    activity
            );
            locationProvider.setDependencies(getMapListener(), getFusedLocationProviderClient(), getLocationCallback());
        }
        return locationProvider;
    }

    public View.OnClickListener getOnMyLocationClockListener() {
        if (onMyLocationClockListener == null) {
            onMyLocationClockListener = new OnMyLocationClickListener(getLocationProvider());
        }
        return onMyLocationClockListener;
    }

    public InfoWindowAdapter getInfoWindowAdapter() {
        if (infoWindowAdapter == null) {
            infoWindowAdapter = new InfoWindowAdapter(activity, getInfoWindowRenderer(activity));
        }
        return infoWindowAdapter;
    }

    public LocationCallback getLocationCallback() {
        if (locationCallback == null) {
            locationCallback = new LocationCallback(){
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    for (Location location : locationResult.getLocations()) {
                        Log.i("MainActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                        getLocationProvider().setLocation(location);
                    }
                }
            };
        }
        return locationCallback;
    }

    public GoogleApiClient getGoogleApiClient() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(activity)
                    .addConnectionCallbacks(getConnectionCallbacks())
                    .addOnConnectionFailedListener(getConnectionFailedListener())
                    .addApi(LocationServices.API)
                    .build();
            googleApiClient.connect();
        }
        return googleApiClient;
    }

    public GoogleApiClient.ConnectionCallbacks getConnectionCallbacks() {
        if (connectionCallbacks == null) {
            connectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
                @Override
                public void onConnected(@Nullable Bundle bundle) {
                    getLocationProvider().requestLocationUpdates();
                }

                @Override
                public void onConnectionSuspended(int i) {

                }
            };
        }
        return connectionCallbacks;
    }

    public GoogleApiClient.OnConnectionFailedListener getConnectionFailedListener() {
        if (connectionFailedListener == null) {
            connectionFailedListener = connectionResult -> ErrorLogger.log(2, "Connection", "Connection failed");
        }
        return connectionFailedListener;
    }

    public InfoWindowRenderer getInfoWindowRenderer(Activity activity) {
        if (infoWindowRenderer == null) {
            infoWindowRenderer = new InfoWindowRenderer(activity);
        }
        return infoWindowRenderer;
    }

    public OnOptionClickListener getOnFilterOptionClickListener(DSZOApplication dszo) {
        if (onFilterOptionClickListener == null) {
            onFilterOptionClickListener = new OnFilterOptionClickListener(dszo, getMapListener());
        }
        return onFilterOptionClickListener;
    }
}