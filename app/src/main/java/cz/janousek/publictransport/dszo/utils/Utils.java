package cz.janousek.publictransport.dszo.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Point;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import cz.janousek.publictransport.dszo.R;

/**
 * Created by martin on 24.10.17.
 */

public class Utils {

    public static final int SECONDS_IN_HOUR = 3600;
    public static final int SECONDS_IN_MINUTE = 60;

    public static List<String> getListOfRows(String input, String lineTerminator) throws Exception {
        return Arrays.asList(input.split(lineTerminator));
    }


    public static String getStopPassport(String stop) {
        if (stop != null) {
            return stop.substring(0, 3);
        }
        return null;
    }

    public static String getStopColumn(String stop) {
        if (stop != null) {
            return stop.substring(3);
        }
        return null;
    }

    public static String formatTimeInterval(Context context, int seconds) {
        if (seconds < SECONDS_IN_MINUTE) {
           return context.getString(R.string.onTime);
        } else if (seconds < SECONDS_IN_HOUR) {
            return context.getString(R.string.min, seconds / SECONDS_IN_MINUTE);
        } else {
            return context.getString(R.string.hour, seconds / SECONDS_IN_HOUR);
        }
    }

    public static void setBackgroundDrawable(View view, Context context, int background) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable( context.getResources().getDrawable(background) );
        } else {
            view.setBackground( context.getResources().getDrawable(background));
        }
    }

    public static Point rotatePoint(Point point, int angle) {
        float p[] = {point.x, point.y};  //create a new float array representing the point (10, 20)
        rotatePoint(p, angle);
        return new Point(Math.round(p[0]), Math.round(p[1]));
    }

    public static float[] rotatePoint(float[] point, int angle) {
        Matrix mat = new Matrix();  //mat is identity
        mat.postRotate(angle);  //mat is a rotation matrix of ROTATE_ANGLE degrees
        mat.mapPoints(point);
        return point;
    }

    public static Runnable getRunOnUiThreadRunnable(Activity activity, Runnable runnable) {
        return () -> activity.runOnUiThread(runnable);
    }


    public static void runCallbacks(Runnable[] callbacks) {
        if (callbacks != null) {
            for (Runnable runnable : callbacks) {
                runnable.run();
            }
        }
    }

    public static String getDate(Long milliSeconds, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}
