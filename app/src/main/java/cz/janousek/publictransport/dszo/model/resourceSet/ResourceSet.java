package cz.janousek.publictransport.dszo.model.resourceSet;

/**
 * Created by martin on 9.12.17.
 */
public interface ResourceSet {
    int getTextColor();
    int getBackroundDrawable();
    int getIcon();
    int getMapMarkerDrawable();
}
