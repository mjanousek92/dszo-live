package cz.janousek.publictransport.dszo.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cz.janousek.publictransport.dszo.dao.actualInfo.ActualInfoDao;
import cz.janousek.publictransport.dszo.dao.actualInfo.ActualInfoSqlDao;
import cz.janousek.publictransport.dszo.dao.polyline.PolylineDao;
import cz.janousek.publictransport.dszo.dao.polyline.PolylineSqlDao;
import cz.janousek.publictransport.dszo.dao.stop.StopDao;
import cz.janousek.publictransport.dszo.dao.stop.StopSqlDao;
import cz.janousek.publictransport.dszo.dao.vehicle.VehicleDao;
import cz.janousek.publictransport.dszo.dao.vehicle.VehicleSqlDao;
import cz.janousek.publictransport.dszo.db.contract.ActualInfoContract;
import cz.janousek.publictransport.dszo.db.contract.PolylineContract;
import cz.janousek.publictransport.dszo.db.contract.StopContract;
import cz.janousek.publictransport.dszo.db.contract.VehicleContract;

/**
 * Created by martin on 23.10.17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "DSZOLive.db";

    private StopDao stopDao;
    private VehicleDao vehicleDao;
    private ActualInfoDao actualInfoDao;
    private PolylineSqlDao polylineDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(StopContract.StopSql.SQL_CREATE_TABLE);
        db.execSQL(VehicleContract.Sql.SQL_CREATE_TABLE);
        db.execSQL(ActualInfoContract.Sql.SQL_CREATE_TABLE);
        db.execSQL(PolylineContract.Sql.SQL_CREATE_TABLE);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        MigrationUtils.migrate(db, oldVersion, newVersion);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public ActualInfoDao getActualInfoDao() {
        if(actualInfoDao == null) {
            actualInfoDao = new ActualInfoSqlDao(this);
        }
        return actualInfoDao;
    }

    public StopDao getStopDao() {
        if(stopDao == null) {
            stopDao = new StopSqlDao(this);
        }
        return stopDao;
    }

    public VehicleDao getVehicleDao() {
        if(vehicleDao == null) {
            vehicleDao = new VehicleSqlDao(this);
        }
        return vehicleDao;
    }

    public PolylineDao getPolylineDao() {
        if(polylineDao == null) {
            polylineDao = new PolylineSqlDao(this);
        }
        return polylineDao;
    }
}
