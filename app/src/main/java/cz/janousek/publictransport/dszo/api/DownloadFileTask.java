package cz.janousek.publictransport.dszo.api;

import android.os.AsyncTask;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;

import cz.janousek.publictransport.dszo.utils.ErrorLogger;
import cz.janousek.publictransport.dszo.utils.Utils;

import static cz.janousek.publictransport.dszo.utils.Utils.runCallbacks;

/**
 * Created by martin on 23.10.17.
 */

public class DownloadFileTask<T> extends AsyncTask<String, Void, List<T>> {

    private ArrayParser<T> arrayParser;
    private Runnable[] callbacks;
    private ResultRunnable<T> resultRunnable;

    public DownloadFileTask(ArrayParser<T> arrayParser, ResultRunnable resultRunnable, Runnable... callbacks) {
        this.resultRunnable = resultRunnable;
        this.arrayParser = arrayParser;
        this.callbacks = callbacks;
    }

    protected List<T> doInBackground(String... urls) {
        String response = null;
        try {
            response = downloadFile(urls[0]);
        } catch (Exception e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }

        List<T> result = null;
        try {
            result = arrayParser.parse(Utils.getListOfRows(response, urls[0].contains(".txt") ? "\\\r\\\n" : "<br/>"));
        } catch (Exception e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        } finally {
            resultRunnable.setResult(result);
            resultRunnable.run();
        }

        return result;
    }

    private String downloadFile(String link) throws Exception {
        URL url = new URL(link);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        logResponseCode(conn.getResponseCode());
        InputStream in = new BufferedInputStream(conn.getInputStream());
        return IOUtils.toString(in, "UTF-8");
    }

    private void logResponseCode(int responseCode) throws MalformedURLException {
        if (responseCode != HttpURLConnection.HTTP_OK) {
            ErrorLogger.log(ErrorLogger.NORMAL, "Response Code", String.valueOf(responseCode));
            throw new MalformedURLException();
        }
    }

    protected void onPostExecute(List<T> result) {
        if(result != null){
            Log.i(DownloadFileTask.class.getSimpleName(), "sync: result size: " + result.size());
            runCallbacks(callbacks);
        }
    }
}
