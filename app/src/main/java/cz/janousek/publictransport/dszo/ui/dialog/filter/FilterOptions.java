package cz.janousek.publictransport.dszo.ui.dialog.filter;

import android.content.Context;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.db.contract.VehicleContract;
import cz.janousek.publictransport.dszo.ui.dialog.Option;
import cz.janousek.publictransport.dszo.ui.dialog.Options;

import static cz.janousek.publictransport.dszo.model.Vehicle.BUS;
import static cz.janousek.publictransport.dszo.model.Vehicle.TROLLEY;

/**
 * Created by Martin Janousek on 6.1.18.
 * - email: mjanousek92@gmail.com
 */

public class FilterOptions extends Options {
    private static final String CONDITION_BASE = VehicleContract.Entry.TABLE_ALIAS + "." + VehicleContract.Entry.COLUMN_NAME_TRACTION + "='%s'";

    public FilterOptions(Context context) {
        super(context);
        init();
    }

    private void init() {
        options.add(new Option(context.getString(R.string.buses), String.format(CONDITION_BASE, BUS)));
        options.add(new Option(context.getString(R.string.trolleys), String.format(CONDITION_BASE, TROLLEY)));
        options.add(new Option(context.getString(R.string.all), null));
    }
}
