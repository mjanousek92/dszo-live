package cz.janousek.publictransport.dszo.ui.dialog;

import android.content.Context;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by martin on 3.1.18.
 */

public class Options {

    protected final Context context;
    protected List<Option> options;

    public Options(Context context) {
        this.context = context;
        this.options = new ArrayList<>();
    }

    public Option get(int positon) {
        return options.get(positon);
    }

    public List<String> getLabels() {
        return Stream.of(options).map(Option::getLabel).collect(Collectors.toList());
    }
}
