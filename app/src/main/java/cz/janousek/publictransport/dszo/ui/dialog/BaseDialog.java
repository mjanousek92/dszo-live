package cz.janousek.publictransport.dszo.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


import cz.janousek.publictransport.dszo.R;

/**
 * Created by martin on 3.1.18.
 */

public class BaseDialog extends Dialog {

    protected Options options;
    private ListView lvOptions;
    protected TextView tvTitle;
    private OnOptionClickListener onOptionClickListener;

    public BaseDialog(@NonNull Context context, OnOptionClickListener onOptionClickListener) {
        super(context);
        this.onOptionClickListener = onOptionClickListener;
        this.onOptionClickListener.setDialog(this);
        setContentView(R.layout.base_options_dialog);
        tvTitle = findViewById(R.id.tv_title);
    }

    protected void init() {
        ArrayAdapter adapter = new ArrayAdapter<>(super.getContext(), android.R.layout.simple_list_item_1, options.getLabels());
        lvOptions = findViewById(R.id.lv_options);
        lvOptions.setAdapter(adapter);
        onOptionClickListener.setOptions(options);
        lvOptions.setOnItemClickListener(onOptionClickListener);
    }
}
