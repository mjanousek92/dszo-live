package cz.janousek.publictransport.dszo.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.MapView;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.ui.listener.MapListener;

/**
 * Created by martin on 29.10.17.
 */

public class MapFragment extends Fragment {


    private MapListener mapListener;
    MapView mapView;

    public void setDependencies(MapListener mapListener) {
        this.mapListener = mapListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.map, container, false);

        //initialize map
        mapView = rootView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(mapListener);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        mapListener.initTask();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapListener.cancelTask();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    public void expandMarker(String vehicleId) {
        mapListener.setVehicleIdWaitingForExpansion(vehicleId);
        mapListener.expandMarker(vehicleId);
    }
}
