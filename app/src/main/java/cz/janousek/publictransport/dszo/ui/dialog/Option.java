package cz.janousek.publictransport.dszo.ui.dialog;

/**
 * Created by martin on 3.1.18.
 */

public class Option {
    private String label;
    private String sqlCondition;

    public Option(String label, String sqlCondition) {
        this.label = label;
        this.sqlCondition = sqlCondition;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getSqlCondition() {
        return sqlCondition;
    }

    public void setSqlCondition(String sqlCondition) {
        this.sqlCondition = sqlCondition;
    }
}
