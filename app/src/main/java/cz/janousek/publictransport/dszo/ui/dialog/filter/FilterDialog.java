package cz.janousek.publictransport.dszo.ui.dialog.filter;

import android.content.Context;
import android.support.annotation.NonNull;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.ui.dialog.BaseDialog;
import cz.janousek.publictransport.dszo.ui.dialog.OnOptionClickListener;

/**
 * Created by martin on 3.1.18.
 */

public class FilterDialog extends BaseDialog {
    public FilterDialog(@NonNull Context context, OnOptionClickListener onOptionClickListener) {
        super(context, onOptionClickListener);
        tvTitle.setText(context.getString(R.string.filter_by));
        init();
    }

    protected void init(){
        options = new FilterOptions(getContext());
        super.init();
    }
}
