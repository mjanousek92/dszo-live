package cz.janousek.publictransport.dszo.db;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.model.Polyline;

/**
 * Created by Martin Janousek on 8.1.18.
 * - email: mjanousek92@gmail.com
 */

public class PolylineInitiator {

    public List<Polyline> loadPolylines(Context context) {
        List<Polyline> polylines = new LinkedList<>();
        try {
            JSONObject json = new JSONObject(loadFile(context, R.raw.polylines));

            Iterator<String> iter = json.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                JSONArray arrayValues = null;
                try {
                    arrayValues = json.getJSONArray(key);
                } catch (JSONException e) {
                    // Something went wrong!
                }
                polylines.add(new Polyline(key, arrayValues.getString(0), arrayValues.getString(1)));
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return polylines;
    }

    private String loadFile(Context context, int resource) throws IOException {
        InputStream is = context.getResources().openRawResource(resource);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        is.close();
        return sb.toString();
    }
}
