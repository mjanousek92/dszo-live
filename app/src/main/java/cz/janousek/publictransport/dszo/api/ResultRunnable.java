package cz.janousek.publictransport.dszo.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.List;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.dao.Dao;

import static cz.janousek.publictransport.dszo.DSZOApplication.APPLICATION_PREFFERENCES;

/**
 * Created by martin on 23.10.17.
 */

public class ResultRunnable<T> implements Runnable {
    protected List<T> data;
    protected Dao<T> dao;
    protected String lastSyncString;
    private DSZOApplication dszoApplication;
    private int lastResponseHash = -1;

    public ResultRunnable(DSZOApplication dszoApplication) {
        this.dszoApplication = dszoApplication;
    }

    public void setResult(List<T> data) {
        this.data = data;
    }

    @Override
    public void run() {
        if (isNewDataOrNull()) {
            Log.d(getClass().getSimpleName(), "Synchorinization: Data are new.");
            saveDataToDatabase();
            saveCurrentTimeToSharedPref();
            setLastResultHash();
        } else {
            Log.d(getClass().getSimpleName(), "Synchorinization: Data are not new.");
        }
    }

    private void saveCurrentTimeToSharedPref() {
        SharedPreferences.Editor editor = dszoApplication.getSharedPreferences(APPLICATION_PREFFERENCES, Context.MODE_PRIVATE).edit();
        editor.putLong(
                lastSyncString, System.currentTimeMillis()
        );
        editor.apply();
    }

    private void saveDataToDatabase() {
        dao.insertOrUpdateAll(data);
    }

    private boolean isNewDataOrNull() {
        return data != null && data.hashCode() != lastResponseHash;
    }

    public void setLastResultHash() {
        this.lastResponseHash = data.hashCode();
    }
}
