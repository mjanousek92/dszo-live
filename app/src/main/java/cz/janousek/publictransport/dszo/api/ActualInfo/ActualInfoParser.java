package cz.janousek.publictransport.dszo.api.ActualInfo;

import com.crashlytics.android.Crashlytics;

import java.util.LinkedList;
import java.util.List;

import cz.janousek.publictransport.dszo.api.ArrayParser;
import cz.janousek.publictransport.dszo.model.ActualInfo;

/**
 * Created by martin on 24.10.17.
 */

public class ActualInfoParser implements ArrayParser<ActualInfo> {

    @Override
    public List<ActualInfo> parse(List<String> data) {
        List result = new LinkedList();
        for (String line : data) {
            String[] a = line.split(";");
            try {
                ActualInfo actualInfo = new ActualInfo(
                        a[FieldOrder.VEHICLE_NUMBER],
                        a[FieldOrder.STOP],
                        Integer.parseInt(a[FieldOrder.DELAY]),
                        Integer.parseInt(a[FieldOrder.LINE]),
                        Integer.parseInt(a[FieldOrder.NUMBER_OF_SERVICE]),
                        Integer.parseInt(a[FieldOrder.DIRECTION]),
                        a[FieldOrder.DRIVER_NUMBER],
                        a[FieldOrder.START_STOP],
                        a[FieldOrder.END_STOP],
                        a[FieldOrder.LAT],
                        a[FieldOrder.LNG],
                        a[FieldOrder.TIMESTAMP],
                        Integer.parseInt(a[FieldOrder.AZIMUTH])
                );
                result.add(actualInfo);
            } catch (NumberFormatException e) {
                Crashlytics.logException(e);
                e.printStackTrace();
            }
        }
        return result;
    }

    private interface FieldOrder{
        int VEHICLE_NUMBER = 0;
        int STOP = 1;
        int DELAY = 2;
        int LINE = 3;
        int NUMBER_OF_SERVICE = 4;
        int DIRECTION = 5;
        int DRIVER_NUMBER = 6;
        int START_STOP = 7;
        int END_STOP = 8;
        int LNG = 9;
        int LAT = 10;
        int TIMESTAMP = 11;
        int AZIMUTH = 12;
    }
}

