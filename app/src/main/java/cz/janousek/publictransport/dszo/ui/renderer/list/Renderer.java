package cz.janousek.publictransport.dszo.ui.renderer.list;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.List;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.model.CompoundActualInfo;
import cz.janousek.publictransport.dszo.ui.adapter.ListAdapter;
import cz.janousek.publictransport.dszo.ui.listener.NavigationItemListener;
import cz.janousek.publictransport.dszo.ui.renderer.BaseRenderer;

/**
 * Created by martin on 16.12.17.
 */

public class Renderer extends BaseRenderer {
    private final NavigationItemListener navigationItemListener;
    private ViewHolder views;
    private ListAdapter adapter;
    private Activity activity;

    public Renderer(Activity activity, NavigationItemListener navigationItemListener, ListAdapter adapter) {
        this.activity = activity;
        this.navigationItemListener = navigationItemListener;
        this.adapter = adapter;
        init();
    }

    private void init() {
        views = new ViewHolder(activity);

        navigationItemListener.setDrawer(views.dlMainLayout);
    }

    public void setData(List<CompoundActualInfo> allCompoundActualInfo) {
        adapter.setAllActualInfo(allCompoundActualInfo);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void setData(Object data) {
        setData((List<CompoundActualInfo>) data);
    }

    public void setActiveMenuItem() {
        views.nvMenu.setCheckedItem(R.id.nav_list);
    }

    public void render() {

        ActionBarDrawerToggle abdToggle = new ActionBarDrawerToggle(
                activity,
                views.dlMainLayout,
                views.tToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        );

        views.dlMainLayout.setDrawerListener(abdToggle);
        abdToggle.syncState();
        views.nvMenu.setNavigationItemSelectedListener(navigationItemListener);
//        fragmentManager.beginTransaction().replace(R.id.main_container, mapFragment).commit();
//        views.fabLocation.setOnClickListener(onMyLocationClockListener);
    }

    private class ViewHolder {

        DrawerLayout dlMainLayout;
        NavigationView nvMenu;
        Toolbar tToolbar;
        RecyclerView rvList;

        public ViewHolder(Activity activity) {
            init(activity);
        }

        private void init(Activity activity) {
            dlMainLayout = activity.findViewById(R.id.dl_main_layout);
            nvMenu = activity.findViewById(R.id.vn_menu);
            tToolbar = activity.findViewById(R.id.toolbar);
            rvList = activity.findViewById(R.id.rv_list);
            initRecycler(activity);
        }

        private void initRecycler(Activity activity) {
            rvList.setLayoutManager(new LinearLayoutManager(activity));
            rvList.setAdapter(adapter);
            rvList.setItemAnimator(new DefaultItemAnimator());
//            rvList.addItemDecoration(new SpacesItemDecoration(10));
        }
    }
}


