package cz.janousek.publictransport.dszo.api.vehicle;

import java.util.LinkedList;
import java.util.List;

import cz.janousek.publictransport.dszo.api.ArrayParser;
import cz.janousek.publictransport.dszo.model.Vehicle;

/**
 * Created by martin on 24.10.17.
 */

public class VehicleParser implements ArrayParser<Vehicle> {

    @Override
    public List<Vehicle> parse(List<String> data) {
        List result = new LinkedList();
        for (String line : data) {
            String[] a = line.split(";");
            Vehicle vehicle = new Vehicle(
                a[FieldOrder.NUMBER],
                a[FieldOrder.TRACTION],
                a[FieldOrder.TYPE],
                TRUE.equals(a[FieldOrder.LOW_FLOOR]),
                TRUE.equals(a[FieldOrder.AIR_CONDITION]),
                TRUE.equals(a[FieldOrder.WIFI]),
                TRUE.equals(a[FieldOrder.LCD]),
                TRUE.equals(a[FieldOrder.BLIND_SUPPORT])
            );
            result.add(vehicle);
        }
        return result;
    }

    private interface FieldOrder{
        int NUMBER = 0;
        int TRACTION = 1;
        int TYPE = 2;
        int LOW_FLOOR = 3;
        int AIR_CONDITION = 4;
        int WIFI = 5;
        int LCD = 6;
        int BLIND_SUPPORT = 7;
    }
}
