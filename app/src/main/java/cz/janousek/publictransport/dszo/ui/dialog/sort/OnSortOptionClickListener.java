package cz.janousek.publictransport.dszo.ui.dialog.sort;

import android.view.View;
import android.widget.AdapterView;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.db.SqlStatement;
import cz.janousek.publictransport.dszo.db.SqlStatementBuilder;
import cz.janousek.publictransport.dszo.db.contract.ActualInfoContract;
import cz.janousek.publictransport.dszo.ui.dialog.OnOptionClickListener;
import cz.janousek.publictransport.dszo.ui.renderer.BaseRenderer;

/**
 * Created by martin on 3.1.18.
 */

public class OnSortOptionClickListener extends OnOptionClickListener {
    public OnSortOptionClickListener(DSZOApplication dszo, BaseRenderer renderer) {
        super(dszo, renderer);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        SqlStatementBuilder ssb = new SqlStatementBuilder();
        SqlStatement sqlStatement = ssb
                .setSelect(ActualInfoContract.Sql.SQL_GET_ALL_COMPOUND_SELECT_FROM)
                .addCondition(ActualInfoContract.Sql.SQL_GET_ALL_COMPOUND_DEFAULT_WHERE)
                .addOrderBy(options.get(position).getSqlCondition())
                .create();
        renderer.setActiveFilter(options.get(position));
        renderer.setData(dszo.getDatabaseHelper().getActualInfoDao().getAllCompound(sqlStatement));
        dialog.dismiss();
    }
}
