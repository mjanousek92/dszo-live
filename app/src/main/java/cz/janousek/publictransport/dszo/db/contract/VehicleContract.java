package cz.janousek.publictransport.dszo.db.contract;

import android.provider.BaseColumns;

/**
 * Created by martin on 19.11.17.
 */

public class VehicleContract {

    private VehicleContract() {}

    public static class Sql {
        public static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                        Entry.COLUMN_NAME_NUMBER + " TEXT, " +
                        Entry.COLUMN_NAME_TRACTION + " TEXT, " +
                        Entry.COLUMN_NAME_TYPE + " TEXT, " +
                        Entry.COLUMN_NAME_LOW_FLOOR + " BOOLEAN, " +
                        Entry.COLUMN_NAME_AIR_CONDITION + " BOOLEAN, " +
                        Entry.COLUMN_NAME_WIFI + " BOOLEAN, " +
                        Entry.COLUMN_NAME_LCD + " BOOLEAN, " +
                        Entry.COLUMN_NAME_BLIND_SUPPORT + " BOOLEAN, " +
                        "PRIMARY KEY (" + Entry.COLUMN_NAME_NUMBER + ")" +
                        ")";
    }

    public static class Entry implements BaseColumns {
        public static final String TABLE_ALIAS = "vehicle";
        public static final String TABLE_NAME = "Vehicle";
        public static final String COLUMN_NAME_AIR_CONDITION = "airCondition";
        public static final String COLUMN_NAME_BLIND_SUPPORT = "blindSupport";
        public static final String COLUMN_NAME_LCD = "lcd";
        public static final String COLUMN_NAME_LOW_FLOOR = "lowFloor";
        public static final String COLUMN_NAME_NUMBER = "number";
        public static final String COLUMN_NAME_TRACTION = "traction";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_WIFI = "wifi";
    }
}
