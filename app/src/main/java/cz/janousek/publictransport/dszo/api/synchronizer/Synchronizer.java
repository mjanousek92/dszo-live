package cz.janousek.publictransport.dszo.api.synchronizer;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.api.ArrayParser;
import cz.janousek.publictransport.dszo.api.DownloadFileTask;
import cz.janousek.publictransport.dszo.api.ResultRunnable;
import cz.janousek.publictransport.dszo.utils.Utils;

import static cz.janousek.publictransport.dszo.DSZOApplication.APPLICATION_PREFFERENCES;

/**
 * Created by martin on 30.12.17.
 */

public class Synchronizer<T> implements Runnable {
    private Runnable[] callbacks;
    protected DSZOApplication dszoApplication;
    protected String lastSyncString;
    protected ArrayParser<T> parser;
    protected ResultRunnable resultRunnable;
    protected long synchoronizationInterval;
    protected String url;

    protected Synchronizer(DSZOApplication dszoApplication) {
        this.dszoApplication = dszoApplication;
    }

    public void sync() {
        if (shouldSync()) {
            Log.d(getClass().getSimpleName(), "Synchorinization: Should sync: Yes");
            DownloadFileTask task = (DownloadFileTask<T>) new DownloadFileTask<T>(
                parser,
                resultRunnable,
                callbacks
            ).execute(url);
        } else {
            Log.d(getClass().getSimpleName(), "Synchorinization: Should sync: No");
            Utils.runCallbacks(callbacks);
        }
    }

    public boolean shouldSync() {
        SharedPreferences sharedPref = dszoApplication.getSharedPreferences(APPLICATION_PREFFERENCES, Context.MODE_PRIVATE);
        Long lastSyncStop = sharedPref.getLong(lastSyncString, 0);
        return System.currentTimeMillis() - lastSyncStop > synchoronizationInterval;
    }

    public void setCallbacks(Runnable[] callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void run() {
        sync();
    }
}
