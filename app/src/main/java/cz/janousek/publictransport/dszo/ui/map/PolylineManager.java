package cz.janousek.publictransport.dszo.ui.map;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import java.util.List;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.ui.activity.BaseActivity;

/**
 * Created by Martin Janousek on 8.1.18.
 * - email: mjanousek92@gmail.com
 */

public class PolylineManager {
    private final Context context;
    private Polyline polyline = null;
    private int color = Color.parseColor("#8888ee");

    public PolylineManager(Context context) {
        this.context = context;
    }

    public void drawPolyline(GoogleMap map, String encodedPolylane) {
        hidePolyline();
        drawNewPolyline(map, encodedPolylane);
    }

    private void drawNewPolyline(GoogleMap map, String encodedPolylane) {
        if (!TextUtils.isEmpty(encodedPolylane)) {
            List<LatLng> points = PolyUtil.decode(encodedPolylane);
            polyline = map.addPolyline(
                    new PolylineOptions()
                            .color(color)
                            .addAll(points)
                            .width(context.getResources().getDimensionPixelSize(R.dimen.polyline_width)
                            ));
        }
    }

    public void hidePolyline() {
        if (polyline != null) {
            polyline.remove();
        }
    }
}
