package cz.janousek.publictransport.dszo.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.db.PolylineInitiator;

/**
 * Created by martin on 29.10.17.
 */

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initPolylines();

        waitAndStartMapActivity();
    }

    private void waitAndStartMapActivity() {
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(SplashActivity.this, MapActivity.class);
            startActivity(intent);
            finish();
        }, 1000);
    }

    private void initPolylines() {
        PolylineInitiator polylineInitiator = new PolylineInitiator();
        dszo.getDatabaseHelper().getPolylineDao().insertAll(
                polylineInitiator.loadPolylines(this.getBaseContext())
        );
    }
}