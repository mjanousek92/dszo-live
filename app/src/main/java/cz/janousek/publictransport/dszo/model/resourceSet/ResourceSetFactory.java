package cz.janousek.publictransport.dszo.model.resourceSet;

import static cz.janousek.publictransport.dszo.model.Vehicle.BUS;
import static cz.janousek.publictransport.dszo.model.Vehicle.TROLLEY;

/**
 * Created by martin on 10.12.17.
 */

public class ResourceSetFactory {

    private static final ResourceSet busResultSet = new BusResourceSet();
    private static final ResourceSet trolleyResourceSet = new TrolleyResourceSet();

    public static ResourceSet createResourceSet(String type) {
        ResourceSet resourceSet = null;

        if (BUS.equals(type)) {
            resourceSet = busResultSet;
        } else if (TROLLEY.equals(type)) {
            resourceSet = trolleyResourceSet;
        }
        return resourceSet;
    }

}
