package cz.janousek.publictransport.dszo.ui.activity;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.api.synchronizer.AllDataSynchronizer;
import cz.janousek.publictransport.dszo.ui.dialog.filter.FilterDialog;
import cz.janousek.publictransport.dszo.ui.dialog.sort.SortDialog;
import cz.janousek.publictransport.dszo.ui.factories.list.Factories;
import cz.janousek.publictransport.dszo.ui.renderer.list.Renderer;

import static cz.janousek.publictransport.dszo.utils.Utils.getRunOnUiThreadRunnable;

public class ListActivity extends BaseActivity {

    private Factories factories;
    private Renderer renderer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        factories = new Factories(this);
        renderer = factories.getRenderer();
        renderer.render();
    }

    @Override
    protected void onResume() {
        super.onResume();

        synchonizeData();

        renderer.setActiveMenuItem();
    }

    private void synchonizeData() {
        AsyncTask.execute(() -> {
            AllDataSynchronizer synchronizer = new AllDataSynchronizer(dszo);
            synchronizer.setCallbacks(new Runnable[]{getRunOnUiThreadRunnable(ListActivity.this, getRenderDataRunnable())});
            synchronizer.sync(true);
        });

    }

    private Runnable getRenderDataRunnable() {
        return () -> renderer.setData(dszo.getDatabaseHelper().getActualInfoDao().getAllCompound());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        Dialog dialog;
        switch (item.getItemId()) {
            case R.id.sort_by:
                dialog = new SortDialog(this, factories.getOnSortOptionClickListener(dszo));
                dialog.show();
                return true;
            case R.id.filter_by:
                dialog = new FilterDialog(this, factories.getOnFilterOptionClickListener(dszo));
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
