package cz.janousek.publictransport.dszo.ui.map;

import android.app.Activity;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import cz.janousek.publictransport.dszo.R;

/**
 * Created by martin on 9.11.17.
 */

public class InfoWindowAdapter implements GoogleMap.InfoWindowAdapter {


    private Activity activity;
    private InfoWindowRenderer renderer;

    public InfoWindowAdapter(Activity activity, InfoWindowRenderer renderer) {
        this.activity = activity;
        this.renderer = renderer;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = activity.getLayoutInflater().inflate(R.layout.map_info_window_content, null);
        renderer.render(view, marker);
        return view;
    }
}
