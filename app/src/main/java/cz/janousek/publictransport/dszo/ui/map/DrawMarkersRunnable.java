package cz.janousek.publictransport.dszo.ui.map;

import android.content.Context;
import android.content.SharedPreferences;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.db.SqlStatement;
import cz.janousek.publictransport.dszo.db.SqlStatementBuilder;
import cz.janousek.publictransport.dszo.db.contract.ActualInfoContract;
import cz.janousek.publictransport.dszo.ui.listener.MapListener;
import cz.janousek.publictransport.dszo.utils.Utils;

import static cz.janousek.publictransport.dszo.DSZOApplication.APPLICATION_PREFFERENCES;

/**
 * Created by martin on 30.12.17.
 */

public class DrawMarkersRunnable implements Runnable {

    private final DSZOApplication dszo;
    private final MapListener mapListener;

    public DrawMarkersRunnable(DSZOApplication dszo, MapListener mapListener) {
        this.dszo = dszo;
        this.mapListener = mapListener;
    }

    @Override
    public void run() {
        SqlStatementBuilder ssb = new SqlStatementBuilder();
        String condition = mapListener.getOption() != null ? mapListener.getOption().getSqlCondition() : null;

        SqlStatement sqlStatement = ssb
                .setSelect(ActualInfoContract.Sql.SQL_GET_ALL_COMPOUND_SELECT_FROM)
                .addCondition(ActualInfoContract.Sql.SQL_GET_ALL_COMPOUND_DEFAULT_WHERE)
                .addCondition(condition)
                .create();
        mapListener.setData(dszo.getDatabaseHelper().getActualInfoDao().getAllCompound(sqlStatement));

        updateLastTimeOfDrawingMarkers();
    }

    private void updateLastTimeOfDrawingMarkers() {
        SharedPreferences sharedPref = dszo.getSharedPreferences(APPLICATION_PREFFERENCES, Context.MODE_PRIVATE);
        String lastSyncString = dszo.getString(R.string.LAST_SYNC_ACTUAL_INFO);
        Long lastSyncStop = sharedPref.getLong(lastSyncString, 0);
        mapListener.stopLoading(dszo.getString(R.string.last_time_of_synchronization, Utils.getDate(lastSyncStop, "HH:mm:ss")));
    }
}
