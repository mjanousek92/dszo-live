package cz.janousek.publictransport.dszo.core.action;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import cz.janousek.publictransport.dszo.R;

/**
 * Created by Martin Janousek on 14.1.18.
 * - email: mjanousek92@gmail.com
 */

public class RateDialogAction extends Action {
    private final Action action;
    private AlertDialog.Builder builder;

    public RateDialogAction(Context context, Action action) {
        this.context = context;
        this.action = action;
        init();
    }

    private void init() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
    }

    @Override
    public void invoke() {
        builder.setTitle(R.string.rate_app)
            .setMessage(R.string.rate_building_content)
            .setPositiveButton(R.string.rate, (dialog, which) -> {
                action.invoke();
            })
            .show();
    }
}
