package cz.janousek.publictransport.dszo.ui.factories.list;

import android.app.Activity;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.ui.adapter.ListAdapter;
import cz.janousek.publictransport.dszo.ui.dialog.OnOptionClickListener;
import cz.janousek.publictransport.dszo.ui.dialog.filter.OnFilterOptionClickListener;
import cz.janousek.publictransport.dszo.ui.dialog.sort.OnSortOptionClickListener;
import cz.janousek.publictransport.dszo.ui.listener.NavigationItemListener;
import cz.janousek.publictransport.dszo.ui.renderer.list.Renderer;

/**
 * Created by martin on 22.10.17.
 */

public class Factories {
    private Activity activity;
    private Renderer renderer;
    private NavigationItemListener navigationItemListener;
    private ListAdapter adapter;
    private OnOptionClickListener onSortOptionClickListener;
    private OnFilterOptionClickListener onFilterOptionClickListener;

    public Factories(Activity activity) {
        this.activity = activity;
    }

    public Renderer getRenderer() {
        if (renderer == null) {
            renderer = new Renderer(
                    activity,
                    getNavigationItemListener(),
                    getAdapter()
            );
        }
        return renderer;
    }

    private ListAdapter getAdapter() {
        if (adapter == null) {
            adapter = new ListAdapter(activity);
        }
        return adapter;
    }

    public NavigationItemListener getNavigationItemListener() {
        if(navigationItemListener == null) {
            navigationItemListener = new NavigationItemListener(activity);
        }
        return navigationItemListener;
    }

    public OnOptionClickListener getOnSortOptionClickListener(DSZOApplication dszo) {
        if (onSortOptionClickListener == null) {
            onSortOptionClickListener = new OnSortOptionClickListener(dszo, getRenderer());
        }
        return onSortOptionClickListener;
    }

    public OnOptionClickListener getOnFilterOptionClickListener(DSZOApplication dszo) {
        if (onFilterOptionClickListener == null) {
            onFilterOptionClickListener = new OnFilterOptionClickListener(dszo, getRenderer());
        }
        return onFilterOptionClickListener;
    }
}