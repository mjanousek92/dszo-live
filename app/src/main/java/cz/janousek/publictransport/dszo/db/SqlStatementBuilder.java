package cz.janousek.publictransport.dszo.db;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by martin on 6.1.18.
 */

public class SqlStatementBuilder {
    private List<String> conditions;
    private String select;
    private List<String> orderBys;

    public SqlStatementBuilder() {
        init();
    }

    private void init() {
        conditions = new LinkedList<>();
        orderBys = new LinkedList<>();
    }

    public SqlStatementBuilder addCondition(String condition) {
        if (condition != null) {
            conditions.add(condition);
        }
        return this;
    }

    public SqlStatementBuilder addOrderBy(String orderBy) {
        if (orderBy != null) {
            orderBys.add(orderBy);
        }
        return this;
    }

    public SqlStatement create() {
        return new SqlStatement(select, conditions, orderBys);
    }

    public SqlStatementBuilder setSelect(String select) {
        this.select = select;
        return this;
    }
}
