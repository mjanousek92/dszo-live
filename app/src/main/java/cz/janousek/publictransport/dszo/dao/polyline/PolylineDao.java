package cz.janousek.publictransport.dszo.dao.polyline;

import java.util.List;

import cz.janousek.publictransport.dszo.dao.Dao;
import cz.janousek.publictransport.dszo.model.Polyline;

/**
 * Created by martin on 23.10.17.
 */

public interface PolylineDao extends Dao<Polyline> {
    void insert(Polyline polyline);
    boolean insertAll(List<Polyline> polylines);
}
