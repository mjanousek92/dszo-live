package cz.janousek.publictransport.dszo.api.stop;

import java.util.List;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.api.ResultRunnable;
import cz.janousek.publictransport.dszo.model.Stop;

/**
 * Created by martin on 25.10.17.
 */

public class StopResultRunnable extends ResultRunnable<Stop> {
    private List<Stop> stops;
    private DSZOApplication dszoApplication;

    public StopResultRunnable(DSZOApplication dszoApplication) {
        super(dszoApplication);
        this.dao = dszoApplication.getDatabaseHelper().getStopDao();
        lastSyncString = dszoApplication.getString(R.string.LAST_SYNC_STOP);
    }
}
