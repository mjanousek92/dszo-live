package cz.janousek.publictransport.dszo.api.ActualInfo;

import android.app.Activity;

import java.util.List;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.api.ResultRunnable;
import cz.janousek.publictransport.dszo.model.ActualInfo;
import cz.janousek.publictransport.dszo.ui.listener.MapListener;

/**
 * Created by martin on 25.10.17.
 */

public class ActualInfoResultRunnable extends ResultRunnable<ActualInfo> {
    private List<ActualInfo> allActualInfos;
    private MapListener mapListener;
    private Activity activity;

    public ActualInfoResultRunnable(DSZOApplication dszoApplication) {
        super(dszoApplication);
        this.dao = dszoApplication.getDatabaseHelper().getActualInfoDao();
        lastSyncString = dszoApplication.getString(R.string.LAST_SYNC_ACTUAL_INFO);
    }
}
