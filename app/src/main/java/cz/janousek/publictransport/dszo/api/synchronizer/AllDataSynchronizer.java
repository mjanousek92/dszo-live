package cz.janousek.publictransport.dszo.api.synchronizer;

import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.utils.Utils;

/**
 * Created by martin on 30.12.17.
 */

public class AllDataSynchronizer implements Runnable{

    private List<Synchronizer> synchronizers;
    private Runnable[] callbacks;
    private int callbackCounter;
    private final Object lock = new Object();

    public AllDataSynchronizer(DSZOApplication dszoApplication) {
        this.synchronizers = new LinkedList<>();
        createSynchronizers(dszoApplication);
    }

    private void createSynchronizers(DSZOApplication dszoApplication) {
        SynchronizerFactories factories = new SynchronizerFactories();

        synchronizers.add(factories.getStopSynchronizer(dszoApplication));
        synchronizers.add(factories.getVehicleSynchronizer(dszoApplication));
        synchronizers.add(factories.getActualDataSynchronizer(dszoApplication));
    }

    public void setCallbacks(Runnable[] callbacks) {
        this.callbacks = callbacks;
    }

    public int sync(boolean waitForAll) {
        clearCallbackCounter();

        for (Synchronizer synchronizer : synchronizers) {
            if (waitForAll) {
                synchronizer.setCallbacks(new Runnable[]{getWaitRunnable()});
            }
            synchronizer.sync();
        }
        return 0;
    }

    private void clearCallbackCounter() {
        synchronized (lock) {
            callbackCounter = 0;
        }
    }

    private Runnable getWaitRunnable(){
        return () -> {
            synchronized (lock) {
                Log.d(AllDataSynchronizer.class.getSimpleName(), "sync: getWaitRunnable(), callbacks size:" +
                        callbacks.length + " callback counter: " + (callbackCounter + 1));
                if (++callbackCounter > 2) {
                    Utils.runCallbacks(callbacks);
                }
            }
        };
    }

    @Override
    public void run() {
        sync(true);
    }
}
