package cz.janousek.publictransport.dszo.dao;

import cz.janousek.publictransport.dszo.db.DatabaseHelper;

/**
 * Created by martin on 24.10.17.
 */

public class BaseDao {

    protected final DatabaseHelper dh;

    public BaseDao(DatabaseHelper dh) {
        this.dh = dh;
    }
}
