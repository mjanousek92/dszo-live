package cz.janousek.publictransport.dszo.model.resourceSet;

import cz.janousek.publictransport.dszo.R;

/**
 * Created by martin on 9.12.17.
 */

public class TrolleyResourceSet implements ResourceSet {
    @Override
    public int getTextColor() {
        return R.color.dszoBlue;
    }

    @Override
    public int getBackroundDrawable() {
        return R.drawable.rouded_corder_yellow;
    }

    @Override
    public int getIcon() {
        return R.drawable.ic_trolley_bus;
    }

    @Override
    public int getMapMarkerDrawable() {
        return R.drawable.ic_arrow_yellow;
    }
}
