package cz.janousek.publictransport.dszo.utils;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

/**
 * Created by martin on 3.12.17.
 */

public class ErrorLogger {
    public static final int HIGH = 1;
    public static final int NORMAL = 2;
    public static final int LOW = 3;

    public static void log(int priority, String tag, String msg) {
        Log.e(tag, msg);
        Crashlytics.log(priority, tag, msg);
    }
}
