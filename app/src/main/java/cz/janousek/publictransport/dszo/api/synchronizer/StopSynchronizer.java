package cz.janousek.publictransport.dszo.api.synchronizer;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.api.stop.StopParser;
import cz.janousek.publictransport.dszo.model.Stop;

import static cz.janousek.publictransport.dszo.utils.MilisecondIntervals.WEEK;

/**
 * Created by martin on 25.10.17.
 */

public class StopSynchronizer extends Synchronizer<Stop> {

    private static final long SYNCHRONIZATION_INTERVAL = WEEK;

    public StopSynchronizer(DSZOApplication dszoApplication) {
        super(dszoApplication);
        resultRunnable = dszoApplication.getStopResultRunnable();
        parser = new StopParser();
        url = "https://www.dszo.cz/online/zastavky.php";
        lastSyncString = dszoApplication.getString(R.string.LAST_SYNC_STOP);
        synchoronizationInterval = SYNCHRONIZATION_INTERVAL;
    }
}
