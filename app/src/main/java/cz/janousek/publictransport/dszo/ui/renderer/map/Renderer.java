package cz.janousek.publictransport.dszo.ui.renderer.map;

/**
 * Created by martin on 22.10.17.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.model.CompoundActualInfo;
import cz.janousek.publictransport.dszo.model.resourceSet.ResourceSet;
import cz.janousek.publictransport.dszo.model.resourceSet.ResourceSetFactory;
import cz.janousek.publictransport.dszo.ui.fragment.MapFragment;
import cz.janousek.publictransport.dszo.ui.listener.NavigationItemListener;
import cz.janousek.publictransport.dszo.utils.Utils;

import static cz.janousek.publictransport.dszo.model.Vehicle.VEHICLE_ID;

/**
 * Created by martin on 14.11.16.
 */

public class Renderer {
    private final NavigationItemListener navigationItemListener;
    private View.OnClickListener onMyLocationClockListener;
    private MapFragment mapFragment;
    private ViewHolder views;
    private Activity activity;

    private FragmentManager fragmentManager;

    public Renderer(Activity activity, NavigationItemListener navigationItemListener, MapFragment mapFragment, FragmentManager fragmentManager, View.OnClickListener onMyLocationClockListener) {
        this.activity = activity;
        this.navigationItemListener = navigationItemListener;
        this.mapFragment = mapFragment;
        this.fragmentManager = fragmentManager;
        this.onMyLocationClockListener = onMyLocationClockListener;
        init();
    }

    private void init() {
        views = new ViewHolder(activity);

        navigationItemListener.setDrawer(views.dlMainLayout);
    }

    public void render() {

        ActionBarDrawerToggle abdToggle = new ActionBarDrawerToggle(
            activity,
            views.dlMainLayout,
            views.tToolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        );

        views.dlMainLayout.setDrawerListener(abdToggle);
        abdToggle.syncState();
        views.nvMenu.setNavigationItemSelectedListener(navigationItemListener);
        fragmentManager.beginTransaction().replace(R.id.main_container, mapFragment).commit();
        views.fabLocation.setOnClickListener(onMyLocationClockListener);
    }

    public boolean isDrawerOper() {
        return views.dlMainLayout.isDrawerOpen(GravityCompat.START);
    }

    public void closeDrawer() {
        views.dlMainLayout.closeDrawer(GravityCompat.START);
    }

    public void showTooltip(boolean show) {
        if (show && views.rlTooltip.getVisibility() != View.VISIBLE) {
//            views.rlTooltip.setVisibility(View.VISIBLE);
            animateShow(views.rlTooltip, 0, 1.0f, View.VISIBLE);
        } else if (!show && views.rlTooltip.getVisibility() != View.GONE) {
            animateShow(views.rlTooltip, views.rlTooltip.getHeight(), 1.0f, View.GONE);
        }
    }

    private void animateShow(View view, int translationY, float alpha, int visibility) {
        view.animate().translationY(translationY).alpha(alpha).setDuration(300).setListener(
            new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    view.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(visibility);
                }
            });
    }

    public void renderTooltip(CompoundActualInfo compoundActualInfo) {
        views.tvOnStop.setText(compoundActualInfo.getOnStop());
        views.tvDelay.setText(Utils.formatTimeInterval(activity, compoundActualInfo.getDelay()));
        views.ivAirCondition.setVisibility(compoundActualInfo.isVehicleAirCondition() ? View.VISIBLE : View.GONE);
        views.ivBlind.setVisibility(compoundActualInfo.isVehicleBlind() ? View.VISIBLE : View.GONE);
        views.ivDisabled.setVisibility(compoundActualInfo.isVehicleLowFloor() ? View.VISIBLE : View.GONE);
        views.ivLcd.setVisibility(compoundActualInfo.isVehicleLcd() ? View.VISIBLE : View.GONE);
        views.ivWifi.setVisibility(compoundActualInfo.isVehicleWifi() ? View.VISIBLE : View.GONE);

        renderVehicle(compoundActualInfo, ResourceSetFactory.createResourceSet(compoundActualInfo.getVehicleTraction()));
    }

    private void renderVehicle(CompoundActualInfo compoundActualInfo, ResourceSet vehicleResourceSet) {
        views.tvVehicleNumber.setText(String.valueOf(compoundActualInfo.getBusLineNumber()));
        views.ivVehicleIcon.setImageResource(vehicleResourceSet.getIcon());
        views.tvVehicleNumber.setTextColor(activity.getResources().getColor(vehicleResourceSet.getTextColor()));
        Utils.setBackgroundDrawable(views.rlVehicleNumber, activity, vehicleResourceSet.getBackroundDrawable());
    }

    public void setActiveMenuItem() {
        views.nvMenu.setCheckedItem(R.id.nav_map);
    }

    public void loadingStart() {
        views.pbLoadingSpinner.setVisibility(View.VISIBLE);
        views.tvSync.setText(activity.getString(R.string.loading));
    }

    public void stopLoading(String messsage) {
        views.pbLoadingSpinner.setVisibility(View.GONE);
        if (messsage != null) {
            views.tvSync.setText(messsage);
        }
    }

    public void expandMarker(Intent intent) {
        String vehicleId = intent.getStringExtra(VEHICLE_ID);
        if (vehicleId != null) {
           mapFragment.expandMarker(vehicleId);
           intent.removeExtra(VEHICLE_ID);
        }
    }

    private class ViewHolder {

        private FloatingActionButton fabLocation;
        private DrawerLayout dlMainLayout;
        private NavigationView nvMenu;
        private Toolbar tToolbar;
        private RelativeLayout rlTooltip, rlVehicleNumber;
        private TextView tvOnStop, tvDelay, tvVehicleNumber, tvSync;
        private ImageView ivAirCondition, ivBlind, ivDisabled, ivLcd, ivWifi, ivVehicleIcon;
        private ProgressBar pbLoadingSpinner;

        public ViewHolder(Activity activity) {
            init(activity);
        }

        private void init(Activity activity) {
            dlMainLayout = activity.findViewById(R.id.dl_main_layout);
            nvMenu = activity.findViewById(R.id.vn_menu);
            tToolbar = activity.findViewById(R.id.toolbar);
            fabLocation = activity.findViewById(R.id.fab_location);

            rlTooltip = activity.findViewById(R.id.rl_tooltip);
            tvOnStop = activity.findViewById(R.id.tv_on_stop);
            tvDelay = activity.findViewById(R.id.tv_delay);

            rlVehicleNumber = activity.findViewById(R.id.rl_vehicle_number);
            tvVehicleNumber = activity.findViewById(R.id.tv_vehicle_number);
            ivVehicleIcon = activity.findViewById(R.id.iv_vehicle_icon);
            ivAirCondition = activity.findViewById(R.id.iv_air_condition);
            ivBlind = activity.findViewById(R.id.iv_blind);
            ivDisabled = activity.findViewById(R.id.iv_disabled);
            ivLcd = activity.findViewById(R.id.iv_lcd);
            ivWifi = activity.findViewById(R.id.iv_wifi);

            pbLoadingSpinner = activity.findViewById(R.id.pb_loading_spinner);
            tvSync = activity.findViewById(R.id.tv_sync);
        }
    }
}

