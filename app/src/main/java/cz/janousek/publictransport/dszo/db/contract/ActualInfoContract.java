package cz.janousek.publictransport.dszo.db.contract;

import android.provider.BaseColumns;

/**
 * Created by martin on 23.12.17.
 */

public final class ActualInfoContract {

    private static final String ACTUAL_INFO_TA_ALIAS = "ai";
    private static final String END_STOP_COLUMN_ALIAS = "endStopColumn";
    private static final String END_STOP_TA_ALIAS = "endStop";
    private static final String ON_STOP_COLUMN_ALIAS = "onStopColumn";
    private static final String ON_STOP_TA_ALIAS = "onStop";
    private static final String POLYLINE_TA_ALIAS = "polyline";

    private ActualInfoContract() {}

    public static class Sql {

        public static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                        Entry.COLUMN_NAME_VEHICLE_NUMBER + " TEXT, " +
                        Entry.COLUMN_NAME_STOP + " TEXT, " +
                        Entry.COLUMN_NAME_DELAY + " INTEGER, " +
                        Entry.COLUMN_NAME_LINE + " INTEGER, " +
                        Entry.COLUMN_NAME_NUMBER_OF_SERVICE + " INTEGER, " +
                        Entry.COLUMN_NAME_DIRECTION + " INTEGER, " +
                        Entry.COLUMN_NAME_DRIVER_NUMBER + " TEXT, " +
                        Entry.COLUMN_NAME_START_STOP + " TEXT, " +
                        Entry.COLUMN_NAME_END_STOP + " TEXT, " +
                        Entry.COLUMN_NAME_LAT + " TEXT, " +
                        Entry.COLUMN_NAME_LNG + " TEXT, " +
                        Entry.COLUMN_NAME_TIME_STAMP + " TEXT, " +
                        Entry.COLUMN_NAME_AZIMUTH + " INTEGER, " +
                        "PRIMARY KEY (" + Entry.COLUMN_NAME_VEHICLE_NUMBER + ")" +
                        ")";

        public static final String SQL_GET_ALL_SELECT_FROM = "SELECT * FROM " + Entry.TABLE_NAME;


        // TODO CREATE VIEW
        public static final String SQL_GET_ALL_COMPOUND_SELECT_FROM =
            "SELECT " +
                Entry.COLUMN_NAME_LINE + ", " +
                Entry.COLUMN_NAME_DELAY + ", " +
                END_STOP_TA_ALIAS + "." + StopContract.StopEntry.COLUMN_NAME_NAME + " " + END_STOP_COLUMN_ALIAS + ", " +
                ON_STOP_TA_ALIAS + "." + StopContract.StopEntry.COLUMN_NAME_NAME + " " + ON_STOP_COLUMN_ALIAS + ", " +
                Entry.COLUMN_NAME_VEHICLE_NUMBER + ", " +
                VehicleContract.Entry.TABLE_ALIAS + "." + VehicleContract.Entry.COLUMN_NAME_TRACTION  + ", " +
                VehicleContract.Entry.TABLE_ALIAS + "." + VehicleContract.Entry.COLUMN_NAME_AIR_CONDITION + ", " +
                VehicleContract.Entry.TABLE_ALIAS + "." + VehicleContract.Entry.COLUMN_NAME_BLIND_SUPPORT + ", " +
                VehicleContract.Entry.TABLE_ALIAS + "." + VehicleContract.Entry.COLUMN_NAME_LCD  + ", " +
                VehicleContract.Entry.TABLE_ALIAS + "." + VehicleContract.Entry.COLUMN_NAME_LOW_FLOOR + ", " +
                VehicleContract.Entry.TABLE_ALIAS + "." + VehicleContract.Entry.COLUMN_NAME_TRACTION + ", " +
                VehicleContract.Entry.TABLE_ALIAS + "." + VehicleContract.Entry.COLUMN_NAME_WIFI + ", " +
                ACTUAL_INFO_TA_ALIAS + "." + Entry.COLUMN_NAME_LAT + ", " +
                ACTUAL_INFO_TA_ALIAS + "." + Entry.COLUMN_NAME_LNG + ", " +
                Entry.COLUMN_NAME_AZIMUTH + ", " +
                POLYLINE_TA_ALIAS + "." + PolylineContract.Entry.COLUMN_NAME_POLYLINE + ", " +
                POLYLINE_TA_ALIAS + "." + PolylineContract.Entry.COLUMN_NAME_LEVELS + " " +

            "FROM " + Entry.TABLE_NAME + " " + ACTUAL_INFO_TA_ALIAS + " " +
            "LEFT JOIN " + StopContract.StopEntry.TABLE_NAME + " " + ON_STOP_TA_ALIAS +
                " ON (" + ON_STOP_TA_ALIAS + "." + StopContract.StopEntry.COLUMN_NAME_PASSPORT + " || " + ON_STOP_TA_ALIAS + "." + StopContract.StopEntry.COLUMN_NAME_COLUMN +
                    ")=" + ACTUAL_INFO_TA_ALIAS + "." + Entry.COLUMN_NAME_STOP + " " +
            "LEFT JOIN " + StopContract.StopEntry.TABLE_NAME + " " + END_STOP_TA_ALIAS +
                " ON (" + END_STOP_TA_ALIAS + "." + StopContract.StopEntry.COLUMN_NAME_PASSPORT + " || " + END_STOP_TA_ALIAS + "." + StopContract.StopEntry.COLUMN_NAME_COLUMN +
                    ")=" + ACTUAL_INFO_TA_ALIAS + "." + Entry.COLUMN_NAME_END_STOP + " " +
            "LEFT JOIN " + PolylineContract.Entry.TABLE_NAME + " " + POLYLINE_TA_ALIAS +
                " ON " + POLYLINE_TA_ALIAS + "." + PolylineContract.Entry.COLUMN_NAME_ID +
                    "=" + ACTUAL_INFO_TA_ALIAS + "." + Entry.COLUMN_NAME_LINE + " " +
            "LEFT JOIN " + VehicleContract.Entry.TABLE_NAME + " " + VehicleContract.Entry.TABLE_ALIAS +
                " ON " + VehicleContract.Entry.TABLE_ALIAS + "." + VehicleContract.Entry.COLUMN_NAME_NUMBER +
                    "=" + ACTUAL_INFO_TA_ALIAS + "." + Entry.COLUMN_NAME_VEHICLE_NUMBER;

        public static final String SQL_GET_ALL_COMPOUND_DEFAULT_WHERE = Entry.COLUMN_NAME_LINE + ">0 AND " + Entry.COLUMN_NAME_LINE + "<900";
    }

    public static class Entry implements BaseColumns {
        public static final String TABLE_NAME = "ActualInfo";
        public static final String COLUMN_NAME_VEHICLE_NUMBER = "vehicleNumber";
        public static final String COLUMN_NAME_STOP = "stop";
        public static final String COLUMN_NAME_DELAY = "delay";
        public static final String COLUMN_NAME_LINE = "line";
        public static final String COLUMN_NAME_NUMBER_OF_SERVICE = "numberOfService";
        public static final String COLUMN_NAME_DIRECTION = "direction";
        public static final String COLUMN_NAME_DRIVER_NUMBER = "driverNumber";
        public static final String COLUMN_NAME_START_STOP = "startStop";
        public static final String COLUMN_NAME_END_STOP = "endStop";
        public static final String COLUMN_NAME_LAT = "lat";
        public static final String COLUMN_NAME_LNG = "lng";
        public static final String COLUMN_NAME_TIME_STAMP = "timeStamp";
        public static final String COLUMN_NAME_AZIMUTH = "azimuth";
    }
}
