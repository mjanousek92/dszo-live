package cz.janousek.publictransport.dszo.dao.actualInfo;

import java.util.List;

import cz.janousek.publictransport.dszo.dao.Dao;
import cz.janousek.publictransport.dszo.db.SqlStatement;
import cz.janousek.publictransport.dszo.model.ActualInfo;
import cz.janousek.publictransport.dszo.model.CompoundActualInfo;

/**
 * Created by martin on 23.10.17.
 */

public interface ActualInfoDao extends Dao<ActualInfo> {
    void add(ActualInfo stop);
//    List<ActualInfo> getAll();
//    List<ActualInfo> getAll(SqlStatement sqlStatement);
    List<CompoundActualInfo> getAllCompound();
    List<CompoundActualInfo> getAllCompound(SqlStatement sqlStatement);
}
