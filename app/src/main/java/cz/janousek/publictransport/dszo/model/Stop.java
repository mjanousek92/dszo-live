package cz.janousek.publictransport.dszo.model;

/**
 * Created by martin on 23.10.17.
 */

public class Stop {
    private String passport;
    private String column;
    private String name;
    private String lat;
    private String lng;
    private boolean onRequest;
    private boolean onRequestInSpecificTime;
    private boolean frontDoorOnly;
    private boolean ticketMachine;
    private String zone;
    private String stateStopNumber;

    public Stop(
        String passport,
        String column,
        String name,
        String lat,
        String lng,
        boolean onRequest,
        boolean onRequestInSpecificTime,
        boolean frontDoorOnly,
        boolean ticketMachine,
        String zone,
        String stateStopNumber
    ) {
        this.passport = passport;
        this.column = column;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.onRequest = onRequest;
        this.onRequestInSpecificTime = onRequestInSpecificTime;
        this.frontDoorOnly = frontDoorOnly;
        this.ticketMachine = ticketMachine;
        this.zone = zone;
        this.stateStopNumber = stateStopNumber;
    }

    public String getPassport() {
        return passport;
    }

    public String getColumn() {
        return column;
    }

    public String getName() {
        return name;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public boolean isOnRequest() {
        return onRequest;
    }

    public boolean isOnRequestInSpecificTime() {
        return onRequestInSpecificTime;
    }

    public boolean isFrontDoorOnly() {
        return frontDoorOnly;
    }

    public boolean isTicketMachine() {
        return ticketMachine;
    }

    public String getZone() {
        return zone;
    }

    public String getStateStopNumber() {
        return stateStopNumber;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public void setOnRequest(boolean onRequest) {
        this.onRequest = onRequest;
    }

    public void setOnRequestInSpecificTime(boolean onRequestInSpecificTime) {
        this.onRequestInSpecificTime = onRequestInSpecificTime;
    }

    public void setFrontDoorOnly(boolean frontDoorOnly) {
        this.frontDoorOnly = frontDoorOnly;
    }

    public void setTicketMachine(boolean ticketMachine) {
        this.ticketMachine = ticketMachine;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public void setStateStopNumber(String stateStopNumber) {
        this.stateStopNumber = stateStopNumber;
    }
}
