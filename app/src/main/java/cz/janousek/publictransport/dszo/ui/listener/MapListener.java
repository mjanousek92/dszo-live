package cz.janousek.publictransport.dszo.ui.listener;

import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cz.janousek.publictransport.dszo.LocationProvider;
import cz.janousek.publictransport.dszo.api.synchronizer.AllDataSynchronizer;
import cz.janousek.publictransport.dszo.model.CompoundActualInfo;
import cz.janousek.publictransport.dszo.model.MarkerTag;
import cz.janousek.publictransport.dszo.ui.activity.BaseActivity;
import cz.janousek.publictransport.dszo.ui.map.DrawMarkersRunnable;
import cz.janousek.publictransport.dszo.ui.map.InfoWindowAdapter;
import cz.janousek.publictransport.dszo.ui.map.MarkerManager;
import cz.janousek.publictransport.dszo.ui.map.PolylineManager;
import cz.janousek.publictransport.dszo.ui.renderer.BaseRenderer;
import cz.janousek.publictransport.dszo.ui.renderer.map.Renderer;
import cz.janousek.publictransport.dszo.utils.MilisecondIntervals;

/**
 * Created by martin on 28.10.17.
 */

public class MapListener extends BaseRenderer implements
        OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMapClickListener
{
    private static final LatLng ZLIN_LOCATION = new LatLng(49.224456, 17.666458);
    private static final float DEFAULT_ZOOM = 13;

    private BaseActivity activity;
    private GoogleMap map;
    private InfoWindowAdapter infoWindowAdapter;
    private LocationProvider locationProvider;
    private MarkerManager markerManager;
    private final Object lock = new Object();
    private PolylineManager polylineManager;
    private Renderer renderer;
    private Timer timer;
    private String vehicleIdWaitingForExpansion;

    public MapListener(BaseActivity activity) {
        this.activity = activity;
    }

    public void setDependencies(
        LocationProvider locationProvider,
        Renderer renderer,
        InfoWindowAdapter infoWindowAdapter
    ) {
        this.locationProvider = locationProvider;
        this.renderer = renderer;
        this.infoWindowAdapter = infoWindowAdapter;

        init();
    }

    private void init() {
        this.markerManager = new MarkerManager(activity);
        this.polylineManager = new PolylineManager(activity);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        map.setMinZoomPreference(6.0f);
        map.setMaxZoomPreference(18.0f);
        map.getUiSettings().setMapToolbarEnabled(false);
        map.setOnMyLocationButtonClickListener(this);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setInfoWindowAdapter(infoWindowAdapter);
        map.setOnMarkerClickListener(this);
        map.setOnMapClickListener(this);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(ZLIN_LOCATION, DEFAULT_ZOOM));

        initTask();
    }

    public void initTask() {
        synchronized (lock) {
            if (timer == null) {
                final Handler handler = new Handler();
                timer = new Timer();
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        AllDataSynchronizer synchronizer = new AllDataSynchronizer(activity.getDszo());
                        synchronizer.setCallbacks(new Runnable[]{getDrawMarkersRunnable()});
                        handler.post(synchronizer);
                    }
                };
                timer.schedule(task, 0, 5 * MilisecondIntervals.SECOND); //change value
            }
        }
    }

    @NonNull
    private DrawMarkersRunnable getDrawMarkersRunnable() {
        return new DrawMarkersRunnable(activity.getDszo(), MapListener.this);
    }

    @Override
    public boolean onMyLocationButtonClick() {
        locationProvider.requestLocationUpdates();
        return false;
    }

    public void moveMap(Location location) {
        if(map != null){
            CameraPosition newPosition = CameraPosition.fromLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), DEFAULT_ZOOM);
            map.animateCamera(CameraUpdateFactory.newCameraPosition(newPosition));
        }
    }

    public void enableMyLocation(boolean enable) throws SecurityException{
        if (map != null) {
            map.setMyLocationEnabled(enable);
        }
    }

    private void drawMarkers(List<CompoundActualInfo> compoundActualInfo) {
        markerManager.addMarkers(map, compoundActualInfo, activity);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        MarkerTag markerTag = (MarkerTag) marker.getTag();
        CompoundActualInfo compoundActualInfo = markerTag.getCompoundActualInfo();
        showAndRenderTooltip(compoundActualInfo);
        showPolyline(compoundActualInfo);
        return false;
    }

    private void showPolyline(CompoundActualInfo compoundActualInfo) {
        polylineManager.drawPolyline(map, compoundActualInfo.getPolyline());
    }

    private void showAndRenderTooltip(CompoundActualInfo compoundActualInfo) {
        renderer.showTooltip(true);
        renderer.renderTooltip(compoundActualInfo);
    }

    public void stopLoading(String date) {
        renderer.stopLoading(date);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        renderer.showTooltip(false);
        polylineManager.hidePolyline();
    }

    public void cancelTask() {
        Log.d("MapListener", "Cancel task");
        synchronized (lock) {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        }
    }

    @Override
    public void setData(Object data) {
        this.drawMarkers((List<CompoundActualInfo>) data);
        expandWaitingMarker();
    }

    private void expandWaitingMarker() {
        if (vehicleIdWaitingForExpansion != null) {
           expandMarker(vehicleIdWaitingForExpansion);
           vehicleIdWaitingForExpansion = null;
        }
    }

    public void expandMarker(String vehicleId) {
        Marker marker = markerManager.get(vehicleId);
        if (marker != null) {
            onMarkerClick(marker);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(((MarkerTag) marker.getTag()).getCompoundActualInfo().getLatLng(), DEFAULT_ZOOM));
            marker.showInfoWindow();
            releaseMarkerWaitingForExpansion();
        }
    }

    private void releaseMarkerWaitingForExpansion() {
        vehicleIdWaitingForExpansion = null;
    }

    public void setVehicleIdWaitingForExpansion(String vehicleIdWaitingForExpansion) {
        this.vehicleIdWaitingForExpansion = vehicleIdWaitingForExpansion;
    }
}
