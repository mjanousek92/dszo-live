package cz.janousek.publictransport.dszo.model;

/**
 * Created by martin on 12.11.17.
 */

public class MarkerTag {
    private CompoundActualInfo compoundActualInfo;

    public MarkerTag(CompoundActualInfo compoundActualInfo) {
        this.compoundActualInfo = compoundActualInfo;
    }

    public CompoundActualInfo getCompoundActualInfo() {
        return compoundActualInfo;
    }

    public void setCompoundActualInfo(CompoundActualInfo compoundActualInfo) {
        this.compoundActualInfo = compoundActualInfo;
    }
}
