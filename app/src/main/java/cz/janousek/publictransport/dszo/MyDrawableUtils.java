package cz.janousek.publictransport.dszo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import cz.janousek.publictransport.dszo.model.resourceSet.ResourceSet;
import cz.janousek.publictransport.dszo.utils.Utils;

/**
 * Created by martin on 30.10.17.
 */

public class MyDrawableUtils {

    private Point shadowOffset;
    private Paint textPaint;
    private float textVerticalOffset;

    public MyDrawableUtils(Context context) {
        init(context);
    }

    public BitmapDescriptor writeOnDrawable(Context context, ResourceSet resourceSet, String busLineNumber, int angle) {
        int textColor = context.getResources().getColor(resourceSet.getTextColor());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, resourceSet.getMapMarkerDrawable());
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Drawable shadowDrawable = ContextCompat.getDrawable(context, R.drawable.ic_arrow_shadow);
        shadowDrawable.setBounds(0, 0, shadowDrawable.getIntrinsicWidth(), shadowDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        float halfWidth = bitmap.getWidth()/2;
        textPaint.setColor(textColor);
        Point point = Utils.rotatePoint(shadowOffset, -angle);

        canvas.translate(point.x, point.y);
        shadowDrawable.draw(canvas);
        canvas.translate(-point.x, -point.y);
        vectorDrawable.draw(canvas);

        canvas.rotate(-angle, halfWidth, halfWidth);
        canvas.drawText(busLineNumber, halfWidth, halfWidth - textVerticalOffset, textPaint);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void init(Context context) {
        initTextPaint(context);
        shadowOffset = new Point(0, context.getResources().getDimensionPixelSize(R.dimen.shadow_offset));
    }

    private void initTextPaint(Context context) {
        textPaint = new Paint();
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.arrow_size));
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        textVerticalOffset = ((textPaint.descent() + textPaint.ascent()) / 2);
    }
}
