package cz.janousek.publictransport.dszo.ui.dialog;

import android.widget.AdapterView;

import cz.janousek.publictransport.dszo.DSZOApplication;
import cz.janousek.publictransport.dszo.ui.renderer.BaseRenderer;

/**
 * Created by martin on 3.1.18.
 */

public abstract class OnOptionClickListener implements AdapterView.OnItemClickListener {
    protected DSZOApplication dszo;
    protected BaseRenderer renderer;
    protected Options options;
    protected BaseDialog dialog;

    public OnOptionClickListener(DSZOApplication dszo, BaseRenderer renderer) {
        this.dszo = dszo;
        this.renderer = renderer;
    }

    public void setOptions(Options options) {
        this.options = options;
    }

    public void setDialog(BaseDialog dialog) {
        this.dialog = dialog;
    }
}
