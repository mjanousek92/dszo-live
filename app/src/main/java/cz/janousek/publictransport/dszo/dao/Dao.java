package cz.janousek.publictransport.dszo.dao;

import java.util.List;

/**
 * Created by martin on 30.12.17.
 */

public interface Dao<T> {
    boolean insertOrUpdateAll(List<T> data);
}
