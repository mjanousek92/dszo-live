package cz.janousek.publictransport.dszo.core.action;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Martin Janousek on 14.1.18.
 * - email: mjanousek92@gmail.com
 */

public abstract class Action {

    protected Context context;
    protected Intent intent;
    protected boolean openingActivity;

    public Intent getIntent() {
        return intent;
    }

    public boolean isOpeningActivity(){
        return openingActivity;
    }

    abstract public void invoke();
}
