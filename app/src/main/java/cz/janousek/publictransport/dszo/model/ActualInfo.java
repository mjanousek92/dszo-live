package cz.janousek.publictransport.dszo.model;

import com.google.android.gms.maps.model.LatLng;

import cz.janousek.publictransport.dszo.utils.Utils;

/**
 * Created by martin on 23.10.17.
 */

public class ActualInfo {
    private String vehicleNumber;
    //format XXXYY, where XXX is passport and YY is column
    private String stop;
    private int delay;
    private int line;
    private int numberOfService;
    private int direction;
    private String driverNumber;
    //format XXXYY, where XXX is passport and YY is column
    private String startStop;
    //format XXXYY, where XXX is passport and YY is column
    private String endStop;
    private String lat;
    private String lng;
    private String timeStamp; // TODO change to LocalDateTime
    private int azimuth;
    private LatLng latLng;

    public ActualInfo(
        String vehicleNumber,
        String stop,
        int delay,
        int line,
        int numberOfService,
        int direction,
        String driverNumber,
        String startStop,
        String endStop,
        String lat,
        String lng,
        String timeStamp,
        int azimuth
    ) {
        this.vehicleNumber = vehicleNumber;
        this.stop = alignToCorrectFormat(stop);
        this.delay = delay;
        this.line = line;
        this.numberOfService = numberOfService;
        this.direction = direction;
        this.driverNumber = driverNumber;
        this.startStop = alignToCorrectFormat(startStop);
        this.endStop = alignToCorrectFormat(endStop);
        this.lat = lat;
        this.lng = lng;
        this.timeStamp = timeStamp;
        this.azimuth = azimuth;

        setLatLng(lat, lng);
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public String getStop() {
        return stop;
    }

    public int getDelay() {
        return delay;
    }

    public int getLine() {
        return line;
    }

    public int getNumberOfService() {
        return numberOfService;
    }

    public int getDirection() {
        return direction;
    }

    public String getDriverNumber() {
        return driverNumber;
    }

    public String getStartStop() {
        return startStop;
    }

    public String getEndStop() {
        return endStop;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public int getAzimuth() {
        return azimuth;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public void setNumberOfService(int numberOfService) {
        this.numberOfService = numberOfService;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public void setDriverNumber(String driverNumber) {
        this.driverNumber = driverNumber;
    }

    public void setStartStop(String startStop) {
        this.startStop = startStop;
    }

    public void setEndStop(String endStop) {
        this.endStop = endStop;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setAzimuth(int azimuth) {
        this.azimuth = azimuth;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    private void setLatLng(String lat, String lng) {
        this.latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
    }

    public String getEndStopPassport() {
        return Utils.getStopPassport(endStop);
    }

    public String getEndStopColumn() {
        return Utils.getStopColumn(endStop);
    }

    public String getStopPassport() {
        return Utils.getStopPassport(stop);
    }

    public String getStopColumn() {
        return Utils.getStopColumn(stop);
    }

    //TODO move to utils
    private String alignToCorrectFormat(String stop) {
        if(stop != null && stop.length() < 5) {
            StringBuilder stopBuilder = new StringBuilder(stop);
            while (stopBuilder.length() < 5) {
                stopBuilder.insert(0, "0");
            }
            return stopBuilder.toString();
        }
        return stop;
    }

    // AUTO-GENERATED
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActualInfo that = (ActualInfo) o;

        if (delay != that.delay) return false;
        if (line != that.line) return false;
        if (numberOfService != that.numberOfService) return false;
        if (direction != that.direction) return false;
        if (azimuth != that.azimuth) return false;
        if (!vehicleNumber.equals(that.vehicleNumber)) return false;
        if (!stop.equals(that.stop)) return false;
        if (!driverNumber.equals(that.driverNumber)) return false;
        if (!startStop.equals(that.startStop)) return false;
        if (!endStop.equals(that.endStop)) return false;
        if (!lat.equals(that.lat)) return false;
        if (!lng.equals(that.lng)) return false;
        if (!timeStamp.equals(that.timeStamp)) return false;
        return latLng.equals(that.latLng);
    }

    // AUTO-GENERATED
    @Override
    public int hashCode() {
        int result = vehicleNumber.hashCode();
        result = 31 * result + stop.hashCode();
        result = 31 * result + delay;
        result = 31 * result + line;
        result = 31 * result + numberOfService;
        result = 31 * result + direction;
        result = 31 * result + driverNumber.hashCode();
        result = 31 * result + startStop.hashCode();
        result = 31 * result + endStop.hashCode();
        result = 31 * result + lat.hashCode();
        result = 31 * result + lng.hashCode();
        result = 31 * result + timeStamp.hashCode();
        result = 31 * result + azimuth;
        result = 31 * result + latLng.hashCode();
        return result;
    }
}
