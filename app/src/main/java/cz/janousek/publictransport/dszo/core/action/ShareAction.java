package cz.janousek.publictransport.dszo.core.action;

import android.content.Context;
import android.content.Intent;

import cz.janousek.publictransport.dszo.R;

/**
 * Created by Martin Janousek on 14.1.18.
 * - email: mjanousek92@gmail.com
 */

public class ShareAction extends Action {
    public ShareAction(Context context) {
        this.context = context;
        init();
    }

    private void init() {
        intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.share_message));
        intent.setType("text/plain");
    }

    @Override
    public void invoke() {
        context.startActivity(intent);
    }
}
