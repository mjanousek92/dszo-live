package cz.janousek.publictransport.dszo.ui.listener;

import android.view.View;

import cz.janousek.publictransport.dszo.LocationProvider;

/**
 * Created by martin on 30.10.17.
 */

public class OnMyLocationClickListener implements View.OnClickListener {

    LocationProvider locationProvider;

    public OnMyLocationClickListener(LocationProvider locationProvider) {
        this.locationProvider = locationProvider;
    }

    @Override
    public void onClick(View view) {
        locationProvider.isCenterAfterResponse(true);
        locationProvider.requestLocationUpdates();
    }
}
