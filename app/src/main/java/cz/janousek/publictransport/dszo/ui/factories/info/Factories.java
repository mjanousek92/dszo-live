package cz.janousek.publictransport.dszo.ui.factories.info;

import android.app.Activity;

import cz.janousek.publictransport.dszo.ui.listener.NavigationItemListener;
import cz.janousek.publictransport.dszo.ui.renderer.info.Renderer;

/**
 * Created by martin on 22.10.17.
 */

public class Factories {
    private Activity activity;
    private Renderer renderer;
    private NavigationItemListener navigationItemListener;

    public Factories(Activity activity) {
        this.activity = activity;
    }

    public Renderer getRenderer() {
        if (renderer == null) {
            renderer = new Renderer(
                    activity,
                    getNavigationItemListener()
            );
        }
        return renderer;
    }

    public NavigationItemListener getNavigationItemListener() {
        if(navigationItemListener == null) {
            navigationItemListener = new NavigationItemListener(activity);
        }
        return navigationItemListener;
    }
}