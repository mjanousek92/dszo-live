package cz.janousek.publictransport.dszo.api;

import java.util.List;

/**
 * Created by martin on 23.10.17.
 */

public interface ArrayParser<T> {
    String TRUE = "1";
    List<T> parse(List<String> data);
}
