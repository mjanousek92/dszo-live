package cz.janousek.publictransport.dszo.dao.stop;

import java.util.List;

import cz.janousek.publictransport.dszo.dao.Dao;
import cz.janousek.publictransport.dszo.model.Stop;

/**
 * Created by martin on 23.10.17.
 */

public interface StopDao extends Dao<Stop> {
    List<Stop> getAll();
    void insert(Stop stop);
    boolean insertAll(List<Stop> stops);
    Stop get(String passport, String column);
}
