package cz.janousek.publictransport.dszo.ui.renderer;

import cz.janousek.publictransport.dszo.ui.dialog.Option;

/**
 * Created by Martin Janousek on 7.1.18.
 * - email: mjanousek92@gmail.com
 */

public abstract class BaseRenderer {
    private Option option;

    public abstract void setData(Object data);

    public void setActiveFilter(Option option) {
        this.option = option;
    }

    public Option getOption() {
        return option;
    }
}
