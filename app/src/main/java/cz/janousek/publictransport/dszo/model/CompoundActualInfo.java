package cz.janousek.publictransport.dszo.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by martin on 31.12.17.
 */

public class CompoundActualInfo {

    private int azimuth;
    private int busLineNumber;
    private int delay;
    private String endStop;
    private LatLng latLng;
    private String onStop;
    private String vehicleNumber;
    private boolean vehicleAirCondition;
    private boolean vehicleBlind;
    private boolean vehicleLowFloor;
    private boolean vehicleLcd;
    private String vehicleTraction;
    private boolean vehicleWifi;
    private String polyline;
    private String levels;

    public CompoundActualInfo(
        int busLineNumber,
        int delay,
        String endStop,
        String onStop,
        String vehicleNumber,
        String vehicleTraction,
        boolean vehicleAirCondition,
        boolean vehicleBlind,
        boolean vehicleLowFloor,
        boolean vehicleLcd,
        boolean vehicleWifi,
        String lat,
        String lng,
        int azimuth,
        String polyline,
        String levels
    ) {
        this.azimuth = azimuth;
        this.busLineNumber = busLineNumber;
        this.delay = delay;
        this.endStop = endStop;
        this.onStop = onStop;
        this.vehicleNumber = vehicleNumber;
        this.vehicleAirCondition = vehicleAirCondition;
        this.vehicleBlind = vehicleBlind;
        this.vehicleLcd = vehicleLcd;
        this.vehicleLowFloor = vehicleLowFloor;
        this.vehicleTraction = vehicleTraction;
        this.vehicleWifi = vehicleWifi;
        this.polyline = polyline;
        this.levels = levels;

        setLatLng(lat, lng);
    }

    public int getBusLineNumber() {
        return busLineNumber;
    }

    public void setBusLineNumber(int busLineNumber) {
        this.busLineNumber = busLineNumber;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public String getEndStop() {
        return endStop;
    }

    public void setEndStop(String endStop) {
        this.endStop = endStop;
    }

    public String getOnStop() {
        return onStop;
    }

    public void setOnStop(String onStop) {
        this.onStop = onStop;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleTraction() {
        return vehicleTraction;
    }

    public void setVehicleTraction(String vehicleTraction) {
        this.vehicleTraction = vehicleTraction;
    }

    public boolean isVehicleAirCondition() {
        return vehicleAirCondition;
    }

    public void setVehicleAirCondition(boolean vehicleAirCondition) {
        this.vehicleAirCondition = vehicleAirCondition;
    }

    public boolean isVehicleBlind() {
        return vehicleBlind;
    }

    public void setVehicleBlind(boolean vehicleBlind) {
        this.vehicleBlind = vehicleBlind;
    }

    public boolean isVehicleLowFloor() {
        return vehicleLowFloor;
    }

    public void setVehicleLowFloor(boolean vehicleLowFloor) {
        this.vehicleLowFloor = vehicleLowFloor;
    }

    public boolean isVehicleLcd() {
        return vehicleLcd;
    }

    public void setVehicleLcd(boolean vehicleLcd) {
        this.vehicleLcd = vehicleLcd;
    }

    public boolean isVehicleWifi() {
        return vehicleWifi;
    }

    public void setVehicleWifi(boolean vehicleWifi) {
        this.vehicleWifi = vehicleWifi;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    private void setLatLng(String lat, String lng) {
        this.latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
    }

    public int getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(int azimuth) {
        this.azimuth = azimuth;
    }

    public String getPolyline() {
        return polyline;
    }

    public void setPolyline(String polyline) {
        this.polyline = polyline;
    }

    public String getLevels() {
        return levels;
    }

    public void setLevels(String levels) {
        this.levels = levels;
    }

}
