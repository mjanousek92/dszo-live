package cz.janousek.publictransport.dszo.ui.dialog.sort;

import android.content.Context;
import android.support.annotation.NonNull;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.ui.dialog.BaseDialog;
import cz.janousek.publictransport.dszo.ui.dialog.OnOptionClickListener;

/**
 * Created by martin on 3.1.18.
 */

public class SortDialog extends BaseDialog {
    public SortDialog(@NonNull Context context, OnOptionClickListener onOptionClickListener) {
        super(context, onOptionClickListener);
        tvTitle.setText(context.getString(R.string.sort_by));
        init();
    }

    protected void init(){
        options = new SortOprions(getContext());
        super.init();
    }
}
