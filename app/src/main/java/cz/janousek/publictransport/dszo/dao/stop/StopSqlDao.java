package cz.janousek.publictransport.dszo.dao.stop;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import cz.janousek.publictransport.dszo.dao.BaseDao;
import cz.janousek.publictransport.dszo.db.DatabaseHelper;
import cz.janousek.publictransport.dszo.db.contract.StopContract;
import cz.janousek.publictransport.dszo.model.Stop;

import static cz.janousek.publictransport.dszo.api.ArrayParser.TRUE;

/**
 * Created by martin on 23.10.17.
 */

public class StopSqlDao extends BaseDao implements StopDao {

    public StopSqlDao(DatabaseHelper dh) {
        super(dh);
    }

    @Override
    public List<Stop> getAll() {
        return null;
    }

    @Override
    public void insert(Stop stop) {
        SQLiteDatabase db = dh.getWritableDatabase();

        ContentValues values = createContentValues(stop);

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(StopContract.StopEntry.TABLE_NAME, null, values);
    }

    @Override
    public boolean insertAll(List<Stop> stops) {
        SQLiteDatabase db = dh.getWritableDatabase();

        for (Stop stop : stops) {
            ContentValues values = createContentValues(stop);
            // Insert the new row, returning the primary key value of the new row
            long newRowId = db.insert(StopContract.StopEntry.TABLE_NAME, null, values);
            Log.d("DB", "Inserted id: " + newRowId);
        }
        return true;
    }

    @Override
    public boolean insertOrUpdateAll(List<Stop> stops) {
        SQLiteDatabase db = dh.getWritableDatabase();

        for (Stop stop : stops) {
            ContentValues values = createContentValues(stop);

            // update
            long id = db.update(
                    StopContract.StopEntry.TABLE_NAME,
                    values,
                    StopContract.StopEntry.COLUMN_NAME_PASSPORT + "=? AND " + StopContract.StopEntry.COLUMN_NAME_COLUMN + "=?",
                    new String[]{stop.getPassport(), stop.getColumn()}
            );

            //insert row unless row was updated
            if (id == 0) {
                id = db.insertWithOnConflict(StopContract.StopEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
//                Log.d("DB", "Inserted id: " + id);
            } else {
//                Log.d("DB", "Updated id: " + id);
            }
        }
        return true;
    }

    @Override
    public Stop get(String passport, String column) {
        SQLiteDatabase db = dh.getReadableDatabase();

        //
        String[] projection = {
                StopContract.StopEntry.COLUMN_NAME_PASSPORT,
                StopContract.StopEntry.COLUMN_NAME_COLUMN,
                StopContract.StopEntry.COLUMN_NAME_NAME,
                StopContract.StopEntry.COLUMN_NAME_LAT,
                StopContract.StopEntry.COLUMN_NAME_LNG,
                StopContract.StopEntry.COLUMN_NAME_ON_REQUEST,
                StopContract.StopEntry.COLUMN_NAME_ON_REQUEST_IN_SPECIFICTIME,
                StopContract.StopEntry.COLUMN_NAME_FRONT_DOOR_ONLY,
                StopContract.StopEntry.COLUMN_NAME_TICKET_MACHINE,
                StopContract.StopEntry.COLUMN_NAME_ZONE,
                StopContract.StopEntry.COLUMN_NAME_STATE_STOP_NUMBER
        };

        // Filter results WHERE "title" = 'My Title'
        String selection = StopContract.StopEntry.COLUMN_NAME_PASSPORT + " = ? AND " + StopContract.StopEntry.COLUMN_NAME_COLUMN + " = ?";
        String[] selectionArgs = {passport, column};

        Cursor cursor = db.query(
                StopContract.StopEntry.TABLE_NAME,        // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                      // The sort order
        );

        Stop stop = null;

        if (cursor.moveToFirst()) {
            stop = new Stop(
                    cursor.getString(cursor.getColumnIndex(StopContract.StopEntry.COLUMN_NAME_PASSPORT)),
                    cursor.getString(cursor.getColumnIndex(StopContract.StopEntry.COLUMN_NAME_COLUMN)),
                    cursor.getString(cursor.getColumnIndex(StopContract.StopEntry.COLUMN_NAME_NAME)),
                    cursor.getString(cursor.getColumnIndex(StopContract.StopEntry.COLUMN_NAME_LAT)),
                    cursor.getString(cursor.getColumnIndex(StopContract.StopEntry.COLUMN_NAME_LNG)),
                    TRUE.equals(cursor.getString(cursor.getColumnIndex(StopContract.StopEntry.COLUMN_NAME_ON_REQUEST))),
                    TRUE.equals(cursor.getString(cursor.getColumnIndex(StopContract.StopEntry.COLUMN_NAME_ON_REQUEST_IN_SPECIFICTIME))),
                    TRUE.equals(cursor.getString(cursor.getColumnIndex(StopContract.StopEntry.COLUMN_NAME_FRONT_DOOR_ONLY))),
                    TRUE.equals(cursor.getString(cursor.getColumnIndex(StopContract.StopEntry.COLUMN_NAME_TICKET_MACHINE))),
                    cursor.getString(cursor.getColumnIndex(StopContract.StopEntry.COLUMN_NAME_ZONE)),
                    cursor.getString(cursor.getColumnIndex(StopContract.StopEntry.COLUMN_NAME_STATE_STOP_NUMBER))
            );
        }

        return stop;
    }

    @NonNull
    private ContentValues createContentValues(Stop stop) {
        ContentValues values = new ContentValues();
        values.put(StopContract.StopEntry.COLUMN_NAME_PASSPORT, stop.getPassport());
        values.put(StopContract.StopEntry.COLUMN_NAME_COLUMN, stop.getColumn());
        values.put(StopContract.StopEntry.COLUMN_NAME_NAME, stop.getName());
        values.put(StopContract.StopEntry.COLUMN_NAME_LAT, stop.getLat());
        values.put(StopContract.StopEntry.COLUMN_NAME_LNG, stop.getLng());
        values.put(StopContract.StopEntry.COLUMN_NAME_ON_REQUEST, stop.isOnRequest());
        values.put(StopContract.StopEntry.COLUMN_NAME_ON_REQUEST_IN_SPECIFICTIME, stop.isOnRequestInSpecificTime());
        values.put(StopContract.StopEntry.COLUMN_NAME_FRONT_DOOR_ONLY, stop.isFrontDoorOnly());
        values.put(StopContract.StopEntry.COLUMN_NAME_TICKET_MACHINE, stop.isTicketMachine());
        values.put(StopContract.StopEntry.COLUMN_NAME_ZONE, stop.getZone());
        values.put(StopContract.StopEntry.COLUMN_NAME_STATE_STOP_NUMBER, stop.getStateStopNumber());
        return values;
    }
}
