package cz.janousek.publictransport.dszo.db;

import android.text.TextUtils;

import java.util.List;

/**
 * Created by martin on 5.1.18.
 */

public class SqlStatement {
    private static final String WHERE = " WHERE ";
    private static final String ORDER_BY = " ORDER BY ";
    private static final String ORDER_BY_DELIMITER = ", ";
    private static final String WHERE_DELIMITER = " AND ";

    private List<String> conditions;
    private String select;
    private List<String> orderBys;

    public SqlStatement(String select, List<String> conditions, List<String> orderBys) {
        this.select = select;
        this.conditions = conditions;
        this.orderBys = orderBys;
    }

    private void addCompoundStatementFromExpressions(StringBuilder sb, String keyWord, String delimiter, List<String> expressions) {
        if (expressions.size() > 0) {
            sb.append(keyWord);
            sb.append(TextUtils.join(delimiter, expressions));
        }
    }

    private void appendWhereClause(StringBuilder sb) {
        String keyWord = WHERE;
        String delimiter = WHERE_DELIMITER;
        List<String> expressions = conditions;
        addCompoundStatementFromExpressions(sb, keyWord, delimiter, expressions);
    }

    private void appendOrderByClause(StringBuilder sb) {
        String keyWord = ORDER_BY;
        String delimiter = ORDER_BY_DELIMITER;
        List<String> expressions = orderBys;
        addCompoundStatementFromExpressions(sb, keyWord, delimiter, expressions);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(select);
        appendWhereClause(sb);
        appendOrderByClause(sb);
        return sb.toString();
    }
}
