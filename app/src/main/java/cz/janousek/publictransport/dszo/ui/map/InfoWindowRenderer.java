package cz.janousek.publictransport.dszo.ui.map;

import android.app.Activity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.Marker;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.model.MarkerTag;
import cz.janousek.publictransport.dszo.model.resourceSet.ResourceSet;
import cz.janousek.publictransport.dszo.model.resourceSet.ResourceSetFactory;
import cz.janousek.publictransport.dszo.utils.Utils;

/**
 * Created by martin on 10.12.17.
 */

public class InfoWindowRenderer {

    private final Activity activity;

    public InfoWindowRenderer(Activity activity) {
        this.activity = activity;
    }

    public void render(View view, Marker marker) {

        renderEndStop(view, (MarkerTag) marker.getTag());
        renderVehicleLineNumber(view, (MarkerTag) marker.getTag());

        TextView tvTitle = view.findViewById(R.id.tv_title);
        TextView tvBusLineNumber = view.findViewById(R.id.tv_bus_line_number);

        MarkerTag markerTag = (MarkerTag) marker.getTag();
        if (markerTag != null) {
            tvTitle.setText(markerTag.getCompoundActualInfo().getEndStop());
            tvBusLineNumber.setText(String.valueOf(markerTag.getCompoundActualInfo().getBusLineNumber()));
        }
    }

    private void renderVehicleLineNumber(View view, MarkerTag tag) {
        if (tag != null) {
            TextView tvBusLineNumber = view.findViewById(R.id.tv_bus_line_number);
            RelativeLayout rlBusLineNumber = view.findViewById(R.id.rl_bus_line_number);
            ResourceSet vehicleResourceSet = ResourceSetFactory.createResourceSet(tag.getCompoundActualInfo().getVehicleTraction());

            tvBusLineNumber.setText(String.valueOf(tag.getCompoundActualInfo().getBusLineNumber()));
            tvBusLineNumber.setTextColor(activity.getResources().getColor(vehicleResourceSet.getTextColor()));
            Utils.setBackgroundDrawable(rlBusLineNumber, activity, vehicleResourceSet.getBackroundDrawable());
        }

    }

    private void renderEndStop(View view, MarkerTag tag) {
        TextView tvTitle = view.findViewById(R.id.tv_title);

        if (tag != null) {
            tvTitle.setText(tag.getCompoundActualInfo().getEndStop());
        }
    }

}
