package cz.janousek.publictransport.dszo.core.action;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Martin Janousek on 14.1.18.
 * - email: mjanousek92@gmail.com
 */

public class OpenActivityAction extends Action {

    public OpenActivityAction(Context context, Class cls) {
        this.context = context;
        init(cls);
    }

    private void init(Class cls) {
        intent = new Intent(context, cls);
        openingActivity = true;
    }

    public void invoke() {
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); // TODO flag
        context.startActivity(intent);
    }
}
