package cz.janousek.publictransport.dszo.ui.renderer.info;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.ui.listener.NavigationItemListener;

/**
 * Created by martin on 16.12.17.
 */

public class Renderer {
    private final NavigationItemListener navigationItemListener;
    private ViewHolder views;
    private Activity activity;

    public Renderer(Activity activity, NavigationItemListener navigationItemListener) {
        this.activity = activity;
        this.navigationItemListener = navigationItemListener;
        init();
    }

    private void init() {
        views = new ViewHolder(activity);

        navigationItemListener.setDrawer(views.dlMainLayout);
    }

    public void setActiveMenuItem() {
        views.nvMenu.setCheckedItem(R.id.nav_info);
    }

    public void render() {

        ActionBarDrawerToggle abdToggle = new ActionBarDrawerToggle(
                activity,
                views.dlMainLayout,
                views.tToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        );

        views.dlMainLayout.setDrawerListener(abdToggle);
        abdToggle.syncState();
        views.nvMenu.setNavigationItemSelectedListener(navigationItemListener);
//        fragmentManager.beginTransaction().replace(R.id.main_container, mapFragment).commit();
//        views.fabLocation.setOnClickListener(onMyLocationClockListener);
    }

    private class ViewHolder {

        private DrawerLayout dlMainLayout;
        private NavigationView nvMenu;
        private Toolbar tToolbar;

        public ViewHolder(Activity activity) {
            init(activity);
        }

        private void init(Activity activity) {
            dlMainLayout = activity.findViewById(R.id.dl_main_layout);
            nvMenu = activity.findViewById(R.id.vn_menu);
            tToolbar = activity.findViewById(R.id.toolbar);
        }
    }
}


