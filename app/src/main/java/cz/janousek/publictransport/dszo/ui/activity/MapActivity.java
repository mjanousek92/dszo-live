package cz.janousek.publictransport.dszo.ui.activity;

import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import cz.janousek.publictransport.dszo.LocationProvider;
import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.ui.dialog.filter.FilterDialog;
import cz.janousek.publictransport.dszo.ui.factories.map.Factories;
import cz.janousek.publictransport.dszo.ui.renderer.map.Renderer;

public class MapActivity extends BaseActivity {

    private Factories factories;
    private Renderer renderer;

    private LocationProvider locationProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        factories = new Factories(this, dszo);
        renderer = factories.getRenderer();
        renderer.render();

        locationProvider = factories.getLocationProvider();
        locationProvider.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        if (renderer != null && renderer.isDrawerOper()) {
            renderer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        locationProvider.onResume();
        renderer.setActiveMenuItem();
        renderer.loadingStart();

        renderer.expandMarker(getIntent());
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationProvider.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.map_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        Dialog dialog;
        switch (item.getItemId()) {
            case R.id.filter_by:
                dialog = new FilterDialog(this, factories.getOnFilterOptionClickListener(dszo));
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
//        dszo.getDatabaseHelper().close();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
//        locationProvider.setLocationPermissionGranted(false);
        switch (requestCode) {
            case LocationProvider.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationProvider.requestLocationUpdates();
                }
                break;
            default:
                break;

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
//        locationProvider.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState, outPersistentState);
    }
}
