package cz.janousek.publictransport.dszo.dao.vehicle;

import java.util.List;

import cz.janousek.publictransport.dszo.dao.Dao;
import cz.janousek.publictransport.dszo.model.Vehicle;

/**
 * Created by martin on 23.10.17.
 */

public interface VehicleDao extends Dao<Vehicle> {
    List<Vehicle> getAllVehicles();
    Vehicle get(String number);
}
