package cz.janousek.publictransport.dszo.dao.actualInfo;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.crashlytics.android.Crashlytics;

import java.util.LinkedList;
import java.util.List;

import cz.janousek.publictransport.dszo.db.DatabaseHelper;
import cz.janousek.publictransport.dszo.db.SqlStatement;
import cz.janousek.publictransport.dszo.db.SqlStatementBuilder;
import cz.janousek.publictransport.dszo.db.contract.ActualInfoContract;
import cz.janousek.publictransport.dszo.db.contract.PolylineContract;
import cz.janousek.publictransport.dszo.db.contract.VehicleContract;
import cz.janousek.publictransport.dszo.model.ActualInfo;
import cz.janousek.publictransport.dszo.model.CompoundActualInfo;
import cz.janousek.publictransport.dszo.utils.ErrorLogger;

import static cz.janousek.publictransport.dszo.api.ArrayParser.TRUE;

/**
 * Created by martin on 23.10.17.
 */

public class ActualInfoSqlDao implements ActualInfoDao {

    private final DatabaseHelper databaseHelper;

    public ActualInfoSqlDao(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    @Override
    public void add(ActualInfo stop) {
    }

    @NonNull
    private List<ActualInfo> getActulaInfoObjectsFromCursor(Cursor cursor) {
        List<ActualInfo> data = new LinkedList<>();

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                data.add(new ActualInfo(
                        cursor.getString(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_VEHICLE_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_STOP)),
                        cursor.getInt(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_DELAY)),
                        cursor.getInt(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_LINE)),
                        cursor.getInt(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_NUMBER_OF_SERVICE)),
                        cursor.getInt(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_DIRECTION)),
                        cursor.getString(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_DRIVER_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_START_STOP)),
                        cursor.getString(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_END_STOP)),
                        cursor.getString(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_LAT)),
                        cursor.getString(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_LNG)),
                        cursor.getString(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_TIME_STAMP)),
                        cursor.getInt(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_AZIMUTH))
                ));

                cursor.moveToNext();
            }
        }
        return data;
    }

//    @Override
//    public List<ActualInfo> getAll() {
//        SqlStatement sqlStatement = getAllSqlStatement();
//        return getAll(sqlStatement);
//    }

//    @Override
//    public List<ActualInfo> getAll(SqlStatement sqlStatement) {
//        Cursor cursor = databaseHelper.getReadableDatabase().rawQuery(sqlStatement.toString(), null);
//        List<ActualInfo> data = getActulaInfoObjectsFromCursor(cursor);
//        cursor.close();
//        return data;
//    }

    @Override
    public List<CompoundActualInfo> getAllCompound() {
        SqlStatement sqlStatement = getAllCompoundSqlStatement();
        return getAllCompound(sqlStatement);
    }

    @Override
    public List<CompoundActualInfo> getAllCompound(SqlStatement sqlStatement) {
        List<CompoundActualInfo> data = null;
        try {
            Cursor cursor = databaseHelper.getReadableDatabase().rawQuery(sqlStatement.toString(), null);
            data = getCompoundActualInfoObjectsFromCursor(cursor);
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorLogger.log(ErrorLogger.HIGH, "getAllCompound", e.getMessage());
            Crashlytics.logException(e);
        }
        return data;
    }

    private SqlStatement getAllCompoundSqlStatement() {
        SqlStatementBuilder ssb = new SqlStatementBuilder();
        return ssb
                .setSelect(ActualInfoContract.Sql.SQL_GET_ALL_COMPOUND_SELECT_FROM)
                .addCondition(ActualInfoContract.Sql.SQL_GET_ALL_COMPOUND_DEFAULT_WHERE)
                .create();
    }

    @Override
    public boolean insertOrUpdateAll(List<ActualInfo> allActualInfo) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        deleteAll();

        for (ActualInfo actualInfo : allActualInfo) {
            ContentValues values = createContentValues(actualInfo);
            db.insertWithOnConflict(ActualInfoContract.Entry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        }
        return true;
    }

    private void deleteAll() {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.delete(ActualInfoContract.Entry.TABLE_NAME, null, null);
    }

    @NonNull
    private ContentValues createContentValues(ActualInfo actualInfo) {
        ContentValues values = new ContentValues();
        values.put(ActualInfoContract.Entry.COLUMN_NAME_VEHICLE_NUMBER, actualInfo.getVehicleNumber());
        values.put(ActualInfoContract.Entry.COLUMN_NAME_STOP, actualInfo.getStop());
        values.put(ActualInfoContract.Entry.COLUMN_NAME_DELAY, actualInfo.getDelay());
        values.put(ActualInfoContract.Entry.COLUMN_NAME_LINE, actualInfo.getLine());
        values.put(ActualInfoContract.Entry.COLUMN_NAME_NUMBER_OF_SERVICE, actualInfo.getNumberOfService());
        values.put(ActualInfoContract.Entry.COLUMN_NAME_DIRECTION, actualInfo.getDirection());
        values.put(ActualInfoContract.Entry.COLUMN_NAME_DRIVER_NUMBER, actualInfo.getDriverNumber());
        values.put(ActualInfoContract.Entry.COLUMN_NAME_START_STOP, actualInfo.getStartStop());
        values.put(ActualInfoContract.Entry.COLUMN_NAME_END_STOP, actualInfo.getEndStop());
        values.put(ActualInfoContract.Entry.COLUMN_NAME_LAT, actualInfo.getLat());
        values.put(ActualInfoContract.Entry.COLUMN_NAME_LNG, actualInfo.getLng());
        values.put(ActualInfoContract.Entry.COLUMN_NAME_TIME_STAMP, actualInfo.getTimeStamp());
        values.put(ActualInfoContract.Entry.COLUMN_NAME_AZIMUTH, actualInfo.getAzimuth());
        return values;
    }

    @NonNull
    private List<CompoundActualInfo> getCompoundActualInfoObjectsFromCursor(Cursor cursor) {
        List<CompoundActualInfo> data = new LinkedList<>();

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                data.add(new CompoundActualInfo(
                        cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        TRUE.equals(cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_AIR_CONDITION))),
                        TRUE.equals(cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_BLIND_SUPPORT))),
                        TRUE.equals(cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_LOW_FLOOR))),
                        TRUE.equals(cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_LCD))),
                        TRUE.equals(cursor.getString(cursor.getColumnIndex(VehicleContract.Entry.COLUMN_NAME_WIFI))),
                        cursor.getString(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_LAT)),
                        cursor.getString(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_LNG)),
                        cursor.getInt(cursor.getColumnIndex(ActualInfoContract.Entry.COLUMN_NAME_AZIMUTH)),
                        cursor.getString(cursor.getColumnIndex(PolylineContract.Entry.COLUMN_NAME_POLYLINE)),
                        cursor.getString(cursor.getColumnIndex(PolylineContract.Entry.COLUMN_NAME_LEVELS))
                ));

                cursor.moveToNext();
            }
        }
        return data;
    }














}
