package cz.janousek.publictransport.dszo.core.action;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import cz.janousek.publictransport.dszo.R;

/**
 * Created by Martin Janousek on 14.1.18.
 * - email: mjanousek92@gmail.com
 */

public class RateAction extends Action {
    public RateAction(Context context) {
        this.context = context;
        init();
    }

    private void init() {
        Uri uri = Uri.parse(context.getString(R.string.market_url) + context.getPackageName());
        intent = new Intent(Intent.ACTION_VIEW, uri);
    }

    @Override
    public void invoke() {
        context.startActivity(intent);
    }
}
