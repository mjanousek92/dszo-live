package cz.janousek.publictransport.dszo.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cz.janousek.publictransport.dszo.R;
import cz.janousek.publictransport.dszo.model.CompoundActualInfo;
import cz.janousek.publictransport.dszo.model.resourceSet.ResourceSet;
import cz.janousek.publictransport.dszo.model.resourceSet.ResourceSetFactory;
import cz.janousek.publictransport.dszo.ui.activity.MapActivity;
import cz.janousek.publictransport.dszo.utils.Utils;

import static cz.janousek.publictransport.dszo.model.Vehicle.VEHICLE_ID;

/**
 * Created by martin on 17.12.17.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private final Activity activity;
    private List<CompoundActualInfo> allCompoundActualInfo;

    public ListAdapter(Activity activity) {
        this.activity = activity;
    }

    public void setAllActualInfo(List<CompoundActualInfo> allCompoundActualInfo) {
        this.allCompoundActualInfo = allCompoundActualInfo;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        v.setOnClickListener(vh);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder views, int position) {
        CompoundActualInfo actualInfo = allCompoundActualInfo.get(position);

        views.setId(actualInfo.getVehicleNumber());
        renderOnStop(views, actualInfo);
        renderEndStop(views, actualInfo);
        renderDelay(views, actualInfo);
        renderVehicle(views, actualInfo, ResourceSetFactory.createResourceSet(actualInfo.getVehicleTraction()));
    }

    private void renderDelay(ViewHolder views, CompoundActualInfo actualInfo) {
        views.tvDelay.setText(Utils.formatTimeInterval(views.itemView.getContext(), actualInfo.getDelay()));
    }

    private void renderEndStop(ViewHolder views, CompoundActualInfo actualInfo) {
        views.tvEndStop.setText(actualInfo.getEndStop());
    }

    private void renderOnStop(ViewHolder views, CompoundActualInfo actualInfo) {
        views.tvOnStop.setText(actualInfo.getOnStop());
    }

    private void renderVehicle(ViewHolder views, CompoundActualInfo actualInfo, ResourceSet vehicleResourceSet) {
        views.tvVehicleNumber.setText(actualInfo.getVehicleNumber());
        views.tvBusLineNumber.setText(String.valueOf(actualInfo.getBusLineNumber()));
        views.tvBusLineNumber.setTextColor(activity.getResources().getColor(vehicleResourceSet.getTextColor()));
        views.ivVehicleIcon.setImageResource(vehicleResourceSet.getIcon());
        Utils.setBackgroundDrawable(views.rlBusLineNumber, activity, vehicleResourceSet.getBackroundDrawable());
    }

    @Override
    public int getItemCount() {
        if(allCompoundActualInfo == null) {
            return 0;
        }
        return allCompoundActualInfo.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvBusLineNumber, tvVehicleNumber, tvEndStop, tvOnStop, tvDelay;
        ImageView ivVehicleIcon;
        RelativeLayout rlBusLineNumber;
        private String id;

        public ViewHolder(View vItem) {
            super(vItem);
            rlBusLineNumber = vItem.findViewById(R.id.rl_bus_line_number);
            tvBusLineNumber = vItem.findViewById(R.id.tv_bus_line_number);
            tvDelay = vItem.findViewById(R.id.tv_delay);
            tvEndStop = vItem.findViewById(R.id.tv_end_stop);
            tvOnStop = vItem.findViewById(R.id.tv_on_stop);
            ivVehicleIcon = vItem.findViewById(R.id.iv_vehicle_icon);
            tvVehicleNumber = vItem.findViewById(R.id.tv_vehicle_number);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(activity, MapActivity.class);
            intent.putExtra(VEHICLE_ID, id);
            activity.startActivity(intent);
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
